FROM ubuntu:xenial
#FROM debian:testing

ENV R_HOME /usr/lib/R
RUN mkdir /mnt/sandbox
RUN echo "shell \"/bin/bash\"" > ~/.screenrc
RUN apt-get update && apt-get install -y \
	libssl1.0.0 \
	libtbb2 \
	openjdk-8-jre-headless \
	screen \
	iproute2 \
  r-cran-rjava \
  r-base \
  r-base-dev


WORKDIR /mnt/sandbox
COPY startup.sh ${WORKDIR}/
COPY init.R /tmp/init.R
COPY overlay-network-node/target/overlay-network-node-0.0.1-SNAPSHOT-jar-with-dependencies.jar ${WORKDIR}/
COPY overlay-network-proxy/Debug/overlay-network-proxy ${WORKDIR}/
COPY overlay-network-ping/Debug/overlay-network-ping ${WORKDIR}/
VOLUME /mnt/sandbox

RUN Rscript /tmp/init.R && rm /tmp/init.R

#CMD ["./startup.sh"]

