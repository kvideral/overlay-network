/*----------------------------------------------------------------------------
 * main.cpp
 *
 *  Created on: 17. 2. 2016
 *      Author: Lukáš Kvídera
 *      Jabber:  segfault@dione.zcu.cz
 ---------------------------------------------------------------------------*/

#include <cstring>
#include <cerrno>
#include <cstdio>
#include <thread>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/un.h>
#include <netinet/in.h>
#include <tbb/tbb.h>
#include <unistd.h>

#include "Logger.h"
#include "utils.h"
#include "ICMPSocket.h"
#include "PingService.h"


void handle_udp() {
  int sock = socket(AF_INET6, SOCK_DGRAM, IPPROTO_UDP);

  if (sock < 0) {
    std::perror("Cannot obtain UDP socket!");
    return;
  }

  sockaddr_in6 address {};
  address.sin6_family = AF_INET6;
  address.sin6_addr = in6addr_any;
  address.sin6_port = htons(1082);
  if (bind(sock, reinterpret_cast<const sockaddr*>(&address), sizeof address) < 0) {
    std::perror("Cannot bind UDP socket!");
    return;
  }

  PingService ping_service { sock };
  tbb::parallel_pipeline(
     32,
     tbb::make_filter<void,int>(
       tbb::filter::parallel,
       [sock](tbb::flow_control& fc)
       {
         return sock;
       }
     )
     &
     tbb::make_filter<int,int>(
       tbb::filter::parallel,
         [&ping_service](const int connection)
         {
           sockaddr_storage destination {};
           sockaddr_storage client {};
           socklen_t addrlen = sizeof client;
           Logger::info("Waiting for address... [UDP]");
           recvfrom(connection, &destination, sizeof destination, MSG_WAITALL,
               reinterpret_cast<sockaddr*>(&client), &addrlen);

           ping_service.enqueue(client, destination);
           return connection;
         }
     )
     &
     tbb::make_filter<int,void>(
       tbb::filter::parallel,
         [](const int connection)
         {
           Logger::trace("Closing connection... [UDP]");
         }
     )
   );
}

int main(void)
{
  handle_udp();
}

