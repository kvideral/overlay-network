/*----------------------------------------------------------------------------
 * utils.cpp
 *
 *  Created on: 22. 12. 2015
 *      Author: Lukáš Kvídera
 *      Jabber:  segfault@dione.zcu.cz
 ---------------------------------------------------------------------------*/


#include <string>
#include <sys/socket.h>
#include <arpa/inet.h>

#include "utils.h"


const std::string to_string(const sockaddr_storage& address)
{
  return inet_ntoa(reinterpret_cast<const sockaddr_in&>(address).sin_addr);
}
