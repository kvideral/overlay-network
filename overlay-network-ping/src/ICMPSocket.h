/*----------------------------------------------------------------------------
 * ICMPSocket.h
 *
 *  Created on: 28. 12. 2015
 *      Author: Lukáš Kvídera
 *      Jabber:  segfault@dione.zcu.cz
 ---------------------------------------------------------------------------*/
#ifndef SRC_ICMPSOCKET_H_
#define SRC_ICMPSOCKET_H_

/*
 *
 */
class ICMPSocket
{
  public:
    ICMPSocket();
    ~ICMPSocket();

    bool probe(const sockaddr_storage& dest, const uint16_t id) const;
    uint16_t receive() const;
  private:
    int icmp_socket = -1;
};

#endif /* SRC_ICMPSOCKET_H_ */
