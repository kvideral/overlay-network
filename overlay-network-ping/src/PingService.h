/*----------------------------------------------------------------------------
 * PingQueue.h
 *
 *  Created on: 23. 3. 2016
 *      Author: Lukáš Kvídera
 *      Jabber:  segfault@dione.zcu.cz
 ---------------------------------------------------------------------------*/
#ifndef SRC_PINGSERVICE_H_
#define SRC_PINGSERVICE_H_

#include <atomic>
#include <map>
#include <memory>
#include <chrono>
#include <thread>
#include <tbb/tbb.h>

#include <sys/socket.h>

#include "ICMPSocket.h"


/*
 *
 */
class PingService
{
  public:
    PingService(const int socket);
    void enqueue(const sockaddr_storage& requestor, const sockaddr_storage& destination);


  private:
    struct request {
        sockaddr_storage requestor;
        sockaddr_storage destination;

        request(const sockaddr_storage& requestor, const sockaddr_storage& destination) :
        requestor { requestor },
        destination { destination }
        { }
    };

    struct ping_data {
      decltype(std::chrono::steady_clock::now()) start;
      std::shared_ptr<request> addresses;
    };


    void ping_job(void);
    void ping(const std::shared_ptr<request> request);
    void respond_job(void);
    void respond(const uint16_t id);
    void clear_job(void);

    const int socket;
    std::atomic<uint16_t> counter { };
    tbb::concurrent_unordered_map<uint16_t, ping_data>  pending_requests;
    tbb::concurrent_queue<std::shared_ptr<request>> new_requests;
    ICMPSocket icmp_socket;
    std::thread requests;
    std::thread responses;
    std::thread gc;
};

#endif /* SRC_PINGSERVICE_H_ */
