/*----------------------------------------------------------------------------
 * PingQueue.cpp
 *
 *  Created on: 23. 3. 2016
 *      Author: Lukáš Kvídera
 *      Jabber:  segfault@dione.zcu.cz
 ---------------------------------------------------------------------------*/


#include <linux/icmp.h>
#include <sys/socket.h>
#include <netinet/in.h>

#include "PingService.h"
#include "Logger.h"
#include "utils.h"

PingService::PingService(const int socket) :
socket { socket },
requests { &PingService::ping_job, this },
responses { &PingService::respond_job, this },
gc { &PingService::clear_job, this }
{
}

void PingService::enqueue(const sockaddr_storage& requestor, const sockaddr_storage& destination)
{
  new_requests.push(std::make_shared<request>(requestor, destination));
}

void PingService::ping_job(void)
{
  while (true) {
    std::shared_ptr<request> request;
    if (new_requests.try_pop(request)) {
      ping(request);
    } else {
      usleep(10000);
    }
  }
}

void PingService::ping(const std::shared_ptr<request> request)
{
  const auto start = std::chrono::steady_clock::now();
  const auto id = counter++;

  Logger::debug("Registering ID: " + std::to_string(id));
  pending_requests[id] = { start, request };

  if (!icmp_socket.probe(request->destination, id)) {
    pending_requests.unsafe_erase(id);
    return;
  }

}

void PingService::respond_job(void)
{
  while (true)
  {
    const auto id = icmp_socket.receive();
    respond(id);
  }
}

void PingService::respond(const uint16_t id)
{
  const auto request_iterator = pending_requests.find(id);
  if (request_iterator == pending_requests.end()) {
    Logger::error("ID not found: " + std::to_string(id));
    return;
  }

  const auto request = request_iterator->second;
  pending_requests.unsafe_erase(id);
  auto delay = std::chrono::duration_cast<std::chrono::milliseconds>
                               (std::chrono::steady_clock::now() - request.start).count();
  Logger::debug("Delay -> " + to_string(request.addresses->destination) + " " + std::to_string(delay) + " ms");

  delay = ((long)(htonl(delay & 0xFFFFFFFFL)) << 32) | htonl(delay >> 32);
  sendto(socket, &delay, sizeof delay, MSG_WAITALL,
                reinterpret_cast<const sockaddr*>(&request.addresses->requestor), sizeof request.addresses->requestor);
}


void PingService::clear_job(void) {
  while (true)
  {
    pending_requests.clear();
    sleep(60);
  }
}

