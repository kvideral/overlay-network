/*----------------------------------------------------------------------------
 * ICMPSocket.cpp
 *
 *  Created on: 28. 12. 2015
 *      Author: Lukáš Kvídera
 *      Jabber:  segfault@dione.zcu.cz
 ---------------------------------------------------------------------------*/

#include <array>
#include <stdexcept>
#include <ctime>
#include <cstring>
#include <chrono>

#include <unistd.h>
#include <arpa/inet.h>
#include <sys/socket.h>

#include <netinet/in.h>
#include <linux/icmp.h>
#include <arpa/inet.h>

#include "ICMPSocket.h"
#include "Logger.h"
#include "utils.h"

#include <iostream>

uint16_t checksum(uint16_t *buffer, size_t size);

ICMPSocket::ICMPSocket()
{
   icmp_socket = socket(AF_INET, SOCK_RAW, IPPROTO_ICMP);
   if (icmp_socket < 0) {
     throw std::runtime_error("Cannot open ICMP socket!");
   }

//   icmp_filter filter { 1<<ICMP_ECHOREPLY };
//   if (setsockopt(icmp_socket, SOL_RAW, ICMP_FILTER, &filter, sizeof filter) < 0) {
//     throw std::runtime_error("Cannot filter ICMP!");
//   }

}

ICMPSocket::~ICMPSocket()
{
  close(icmp_socket);
}


bool ICMPSocket::probe(const sockaddr_storage& destination, const uint16_t id) const
{
  Logger::trace("Measuring external ping -> " + to_string(destination));
  std::array<char, 52> buffer { };
  icmphdr& header = reinterpret_cast<icmphdr&>(buffer.front());
  header.type = ICMP_ECHO;
  header.un.echo.id = id;
  header.un.echo.sequence = id;
  header.checksum = checksum(reinterpret_cast<uint16_t*>(&header), sizeof header);

  if (sendto(icmp_socket, &header, sizeof header, 0, reinterpret_cast<const sockaddr*>(&destination), sizeof destination) < 0) {
    std::perror("Cannot send ICMP ECHO!");
    return false;
  }

  return true;
}

uint16_t ICMPSocket::receive() const {
  std::array<char, 52> buffer { };
  icmphdr& header = reinterpret_cast<icmphdr&>(buffer.front());
  if (recvfrom(icmp_socket, &header, buffer.size(), 0, nullptr, nullptr) < 0) {
    std::perror("Cannot receive ICMP REPLY!");
    return UINT16_MAX;
  }

  /** We received IP header with ICMP header, so we have to add offset */
  header = reinterpret_cast<icmphdr&>(*(buffer.begin() + 20));
  switch (header.type)
  {
    case ICMP_ECHOREPLY:
      Logger::info("ICMP_ECHOREPLY");
      break;

    case ICMP_ECHO:
      Logger::info("ICMP_ECHO");
      return UINT16_MAX;

    case ICMP_DEST_UNREACH:
      Logger::info("ICMP_DEST_UNREACH");
      return UINT16_MAX;

    case ICMP_REDIRECT:
      Logger::info("ICMP_REDIRECT");
      return UINT16_MAX;

    case ICMP_TIME_EXCEEDED:
      Logger::info("ICMP_TIME_EXCEEDED");
      return UINT16_MAX;

    default:
      Logger::info("OTHER ICMP");
      return UINT16_MAX;
  }

  return header.un.echo.id;
}

uint16_t checksum(uint16_t *buffer, size_t size)
{
  uint64_t cksum { };
  while(size > 1)
  {
    cksum += *buffer++;
    size -= sizeof(uint16_t);
  }

  if(size)
  {
    cksum += *(uint8_t*) buffer;
  }

  cksum = (cksum >> 16) + (cksum & 0xffff);
  cksum += (cksum >> 16);
  return ~cksum;
}
