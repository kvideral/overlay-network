/*----------------------------------------------------------------------------
 * utils.h
 *
 *  Created on: 22. 12. 2015
 *      Author: Lukáš Kvídera
 *      Jabber:  segfault@dione.zcu.cz
 ---------------------------------------------------------------------------*/
#ifndef SRC_UTILS_H_
#define SRC_UTILS_H_

#include <string>
#include <sys/socket.h>

const std::string to_string(const sockaddr_storage& address);


#endif /* SRC_UTILS_H_ */
