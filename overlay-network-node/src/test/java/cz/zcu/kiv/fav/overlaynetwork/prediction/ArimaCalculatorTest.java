/**
 * ArimaCalculatorTest.java
 * 19. 3. 2016
 */
package cz.zcu.kiv.fav.overlaynetwork.prediction;

import static org.junit.Assert.assertEquals;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Random;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cz.zcu.kiv.fav.overlaynetwork.membership.LinkReport;
import cz.zcu.kiv.fav.overlaynetwork.membership.LinkReport.Delay;

/**
 * @author Lukáš Kvídera
 */
public class ArimaCalculatorTest {

  private static final Logger LOG = LoggerFactory.getLogger(ArimaCalculatorTest.class);

  private static Random rnd = new Random(0);

  @Test
  public void test() throws UnknownHostException {
    final LinkReport report = new LinkReport(InetAddress.getLocalHost());
    for (int i = 0; i < 100; i++) {
      report.add(new Delay((long) (Math.abs(rnd.nextGaussian()) * 1010)));
    }

    final ArimaCalculator c = new ArimaCalculator();
    final Prediction prediction = c.calculate(report.getDelayTimeSerie());
    LOG.info("Prediction: {}", prediction.getAsLong());
    assertEquals("prediction", 779, prediction.getAsLong());
  }

}
