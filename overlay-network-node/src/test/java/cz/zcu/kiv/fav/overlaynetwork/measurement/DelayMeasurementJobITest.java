/**
 * DelayMeasurementJobITest.java
 * 11. 3. 2016
 */
package cz.zcu.kiv.fav.overlaynetwork.measurement;

import java.sql.SQLException;

import org.junit.Test;
import org.quartz.JobExecutionException;

/**
 * @author Lukáš Kvídera
 */
public class DelayMeasurementJobITest {

  @Test
  public void test() throws SQLException, JobExecutionException {
    final DelayMeasurementJob delayMeasurementJob = new DelayMeasurementJob();
    delayMeasurementJob.execute(null);
  }
}
