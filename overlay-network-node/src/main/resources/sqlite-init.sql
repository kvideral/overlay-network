CREATE TABLE IF NOT EXISTS 'main'.'delayseries' (
id                 INTEGER PRIMARY KEY,
ipaddress          TEXT NOT NULL,
delay              INT NOT NULL,
date               TIMESTAMP
DEFAULT CURRENT_TIMESTAMP);

CREATE TABLE IF NOT EXISTS 'main'.'throughputseries' (
id                 INTEGER PRIMARY KEY,
ipaddress          TEXT NOT NULL,
throughput         INT NOT NULL,
date               TIMESTAMP
DEFAULT CURRENT_TIMESTAMP);

CREATE TABLE IF NOT EXISTS 'main'.'linkerrorseries' (
id                 INTEGER PRIMARY KEY,
ipaddress          TEXT NOT NULL,
packetloss         INT NOT NULL,
date               TIMESTAMP
DEFAULT CURRENT_TIMESTAMP);
