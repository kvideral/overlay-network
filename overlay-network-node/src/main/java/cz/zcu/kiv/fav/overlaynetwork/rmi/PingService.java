/**
 * PingService.java
 * 13. 3. 2016
 */
package cz.zcu.kiv.fav.overlaynetwork.rmi;

import java.io.Closeable;
import java.net.InetAddress;
import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * @author Lukáš Kvídera
 */
public interface PingService extends Remote, Closeable {
  long ping(InetAddress destination) throws RemoteException ;
}
