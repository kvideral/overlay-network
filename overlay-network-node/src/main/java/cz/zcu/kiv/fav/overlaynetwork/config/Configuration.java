/**
 * Configuration.java
 * 5. 3. 2016
 */
package cz.zcu.kiv.fav.overlaynetwork.config;


/**
 * @author Lukáš Kvídera
 */
public class Configuration {

  public static final Configuration DEFAULT = new Configuration();
  private int pingInterval;
  private int routeUpdateInterval;
  private int routeCalculationInterval;
  private int routeTTL;

  Configuration() {

  }

  public int getPingInterval() {
    return this.pingInterval;
  }

  public void setPingInterval(final int pingInterval) {
    this.pingInterval = pingInterval;
  }

  public int getRouteUpdateInterval() {
    return this.routeUpdateInterval;
  }

  public void setRouteUpdateInterval(final int routeUpdateInterval) {
    this.routeUpdateInterval = routeUpdateInterval;
  }

  public int getRouteCalculationInterval() {
    return this.routeCalculationInterval;
  }

  public void setRouteCalculationInterval(final int routeCalculationInterval) {
    this.routeCalculationInterval = routeCalculationInterval;
  }

  public int getRouteTTL() {
    return this.routeTTL;
  }

  public void setRouteTTL(final int ttl) {
    this.routeTTL = ttl;
  }
}
