/**
 * HourlyAgregationJob.java
 * 17. 4. 2016
 */
package cz.zcu.kiv.fav.overlaynetwork.measurement;

import java.sql.SQLException;

import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cz.zcu.kiv.fav.overlaynetwork.membership.StatisticalDataDao;

/**
 * @author Lukáš Kvídera
 */
@DisallowConcurrentExecution
public class HourlyAgregationJob implements Job {

  private static final Logger LOG = LoggerFactory.getLogger(HourlyAgregationJob.class);

  @Override
  public void execute(final JobExecutionContext context) throws JobExecutionException {
    LOG.trace("execute(context)");
    try {
      final StatisticalDataDao statisticalDataDao = new StatisticalDataDao();
      statisticalDataDao.computeHourlyAgregation();
    } catch (final SQLException e) {
      throw new JobExecutionException(e);
    }
  }

}
