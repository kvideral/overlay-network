/**
 * ArimaCalculator.java
 * 19. 3. 2016
 */
package cz.zcu.kiv.fav.overlaynetwork.prediction;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.lang.reflect.InvocationTargetException;
import java.time.Instant;
import java.util.List;
import java.util.function.LongSupplier;

import org.rosuda.REngine.REXP;
import org.rosuda.REngine.REXPDouble;
import org.rosuda.REngine.REXPMismatchException;
import org.rosuda.REngine.REngine;
import org.rosuda.REngine.REngineException;
import org.rosuda.REngine.RList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Lukáš Kvídera
 */
public class ArimaCalculator implements PredictionCalculator {

  private static final Logger LOG = LoggerFactory.getLogger(ArimaCalculator.class);
  private static final Prediction DEFAULT = () -> Integer.MAX_VALUE;

  private static final String LOAD_LIBRARY = "library(forecast);";

  private static final String CALCULATE_PREDICTION =
            "arimadata <- auto.arima(data);"
          + "prediction <- forecast(arimadata);"
          ;

  private final REngine re;

  static {
    try {
      REngine.engineForClass("org.rosuda.REngine.JRI.JRIEngine");
    } catch (ClassNotFoundException | NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
      LOG.error("Cannot start R!", e);
    }
  }

  public ArimaCalculator() {
    this.re = REngine.getLastEngine();
  }

  @Override
  public Prediction calculate(final List<? extends LongSupplier> data) {
    String fileName = "/tmp/none";
    try {
      fileName = prepareTemporaryFile(data);
      this.re.parseAndEval(LOAD_LIBRARY);
      this.re.parseAndEval("data <- scan('" + fileName + "',skip=1);");
      this.re.parseAndEval(CALCULATE_PREDICTION);
      final REXP fs = this.re.parseAndEval("summary(prediction);");

      final RList forecast = fs.asList();
      long sum = 0;
      for (int i = 0; i < forecast.size(); ++i) {
        final double delay = ((REXPDouble) forecast.get(i)).asDouble();
        LOG.debug("Forecast {}", delay);
        sum += delay;
      }

      /* hmmm no prediction */
      if (forecast.size() == 0 || sum <= 0) {
        LOG.warn("We cannot predict.");
        return DEFAULT;
      }

      final long calculatedDelay = sum / forecast.size();
      return () -> calculatedDelay;
    } catch (final REngineException | REXPMismatchException | IOException e) {
      LOG.error(e.getLocalizedMessage(), e);
    } finally {
      /* delete file if not needed */
      new File(fileName).delete();
    }

    return DEFAULT;
  }

  private static String prepareTemporaryFile(final List<? extends LongSupplier> data)
      throws FileNotFoundException, IOException {
    final File outputFile = File.createTempFile("overlay-network", Instant.now().toString());
    try (final PrintStream ps = new PrintStream(new FileOutputStream(outputFile))) {
      for (final LongSupplier value : data) {
        ps.println(value.getAsLong());
      }
      return outputFile.getPath();
    }
  }
}
