/**
 * Prediction.java
 * 5. 3. 2016
 */
package cz.zcu.kiv.fav.overlaynetwork.prediction;

import java.util.function.LongSupplier;

/**
 * @author Lukáš Kvídera
 */
public interface Prediction extends LongSupplier {

}
