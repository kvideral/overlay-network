/**
 * StatisticalDataDao.java
 * 11. 3. 2016
 */
package cz.zcu.kiv.fav.overlaynetwork.membership;

import java.net.InetAddress;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cz.zcu.kiv.fav.overlaynetwork.membership.LinkReport.Delay;
import cz.zcu.kiv.fav.overlaynetwork.membership.LinkReport.Throughput;

/**
 * @author Lukáš Kvídera
 */
public class StatisticalDataDao implements StatisticalDataProvider {

  private static final Logger LOG = LoggerFactory.getLogger(StatisticalDataDao.class);
  private static final String JDBC_STRING = "jdbc:sqlite:/tmp/overlay-network.db";

  private final Connection connection;

  static {
    try {
      Class.forName("org.sqlite.JDBC");
      try (final Connection connection = DriverManager.getConnection(JDBC_STRING)) {
        try (final PreparedStatement stmt = connection.prepareStatement(
            "CREATE TABLE IF NOT EXISTS 'main'.'delayseries' ( "
                + "id         INTEGER PRIMARY KEY ,            "
                + "ipaddress  TEXT NOT NULL,                   "
                + "delay      INT NOT NULL,                    "
                + "date       TIMESTAMP                        "
                + "  DEFAULT CURRENT_TIMESTAMP                )")
            ) {
          stmt.executeUpdate();
        }

        try (final PreparedStatement stmt = connection.prepareStatement(
            "CREATE TABLE IF NOT EXISTS 'main'.'delayserieshourly' ( "
                + "id         INTEGER PRIMARY KEY ,            "
                + "ipaddress  TEXT NOT NULL,                   "
                + "delay      INT NOT NULL,                    "
                + "date       TIMESTAMP                        "
                + "  DEFAULT CURRENT_TIMESTAMP                )")
            ) {
          stmt.executeUpdate();
        }

        try (final PreparedStatement stmt = connection.prepareStatement(
            "CREATE TABLE IF NOT EXISTS 'main'.'throughputseries' ( "
                + "id         INTEGER PRIMARY KEY ,                 "
                + "ipaddress  TEXT NOT NULL,                        "
                + "throughput INT NOT NULL,                         "
                + "date       TIMESTAMP                             "
                + "  DEFAULT CURRENT_TIMESTAMP                     )")
            ) {
          stmt.executeUpdate();
        }

        try (final PreparedStatement stmt = connection.prepareStatement(
            "CREATE TABLE IF NOT EXISTS 'main'.'linkerrorseries' ( "
                + "id           INTEGER PRIMARY KEY ,              "
                + "ipaddress    TEXT NOT NULL,                     "
                + "packetloss   INT NOT NULL,                      "
                + "date         TIMESTAMP                          "
                + "  DEFAULT CURRENT_TIMESTAMP                    )")
            ) {
          stmt.executeUpdate();
        }
      }
    } catch (final Exception e) {
      LOG.error("Canot load SQLite3 driver!", e);
      System.exit(-1);
    }
  }

  public StatisticalDataDao() throws SQLException {
    this.connection = DriverManager.getConnection(JDBC_STRING);

  }

  @Override
  public LinkReport getReport(final InetAddress address) throws SQLException {
    LOG.trace("getData(address={})", address);

    final LinkReport report = new LinkReport(address);
    this.readDelays(report, address);
    this.readThroughputs(report, address);
    return report;
  }

  private void readDelays(final LinkReport report, final InetAddress address) throws SQLException {
    LOG.trace("readDelays(report)");
    try (
        final PreparedStatement stmt =
          this.connection.prepareStatement(
              "SELECT * FROM (SELECT delay, date FROM delayseries WHERE ipaddress = ? ORDER BY date DESC LIMIT 100)"
              + "ORDER BY date ASC");
        final PreparedStatement agregated =
          this.connection.prepareStatement(
              "SELECT * FROM (SELECT delay, date FROM delayserieshourly WHERE ipaddress = ? ORDER BY date DESC LIMIT 100)"
              + "ORDER BY date ASC")
        ) {
      stmt.setString(1, address.getHostAddress());

      try (final ResultSet resultSet = stmt.executeQuery()) {
        while (resultSet.next()) {
          report.add(new Delay(resultSet.getLong(1)));
          LOG.debug("SQLite {}", report);
        }
      }

      agregated.setString(1, address.getHostAddress());
      try (final ResultSet resultSet = agregated.executeQuery()) {
        while (resultSet.next()) {
          report.addHourly(new Delay(resultSet.getLong(1)));
          LOG.debug("SQLite {}", report);
        }
      }
    }
  }

  private void readThroughputs(final LinkReport report, final InetAddress address) throws SQLException {
    LOG.trace("readDelays(report)");
    try (final PreparedStatement stmt =
        this.connection.prepareStatement(
            "SELECT * FROM (SELECT " + Long.MAX_VALUE + " / throughput, date FROM throughputseries WHERE ipaddress = ?"
                + " ORDER BY date DESC LIMIT 100)"
                + "ORDER BY date ASC")) {
      stmt.setString(1, address.getHostAddress());

      try (final ResultSet resultSet = stmt.executeQuery()) {
        while (resultSet.next()) {
          report.add(new Throughput(resultSet.getLong(1)));
          LOG.debug("SQLite {}", report);
        }
      }
    }
  }

  @Override
  public void persistDelay(final InetAddress address, final long delay) throws SQLException {
    LOG.trace("persistDelay(address={}, delay={})", address, delay);
    try (final PreparedStatement stmt =
        this.connection.prepareStatement("INSERT into delayseries (ipaddress, delay) VALUES (?,?)")) {
      stmt.setString(1, address.getHostAddress());
      stmt.setLong(2, delay);
      stmt.executeUpdate();
    }
  }

  public void persistDelayHourly(final String address, final long delay) throws SQLException {
    LOG.trace("persistDelay(address={}, delay={})", address, delay);
    try (final PreparedStatement stmt =
        this.connection.prepareStatement("INSERT into delayserieshourly (ipaddress, delay) VALUES (?,?)")) {
      stmt.setString(1, address);
      stmt.setLong(2, delay);
      stmt.executeUpdate();
    }
  }

  @Override
  public void persistThroughput(final InetAddress address, final long throughput) throws SQLException {
    LOG.trace("persistThroughput(address={}, throughput={})", address, throughput);
    try (final PreparedStatement stmt =
        this.connection.prepareStatement("INSERT into throughputseries (ipaddress, throughput) VALUES (?,?)")) {
      stmt.setString(1, address.getHostAddress());
      stmt.setLong(2, throughput);
      stmt.executeUpdate();
    }
  }


  public void computeHourlyAgregation() throws SQLException {
    LOG.trace("computeHourlyAgregation()");
    try (
        final PreparedStatement addressQuery =
        this.connection.prepareStatement(
            "SELECT DISTINCT ipaddress FROM delayseries");

        final PreparedStatement dataQuery =
        this.connection.prepareStatement(
            "SELECT delay FROM delayseries WHERE date > datetime('now','-1 hour') AND ipaddress = ?"
            + "ORDER by date DESC LIMIT 60")) {

      final List<String> addresses = new ArrayList<>();
      try (final ResultSet resultSet = addressQuery.executeQuery()) {
        while (resultSet.next()) {
          addresses.add(resultSet.getString(1));
        }
      }

      for (final String address : addresses) {
        dataQuery.setString(1, address);
        try (final ResultSet resultSet = dataQuery.executeQuery()) {
          long delay = 0;
          int count = 0;
          while (resultSet.next()) {
            delay += resultSet.getLong(1);
            ++count;
          }

          this.persistDelayHourly(address, delay / count);
        }
      }

    }
  }

}
