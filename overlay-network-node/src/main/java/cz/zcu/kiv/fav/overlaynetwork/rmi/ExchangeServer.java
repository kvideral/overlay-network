/**
 * ExchangeServer.java
 * 5. 3. 2016
 */
package cz.zcu.kiv.fav.overlaynetwork.rmi;

import static cz.zcu.kiv.fav.overlaynetwork.utils.RmiNameUtils.getURL;

import java.net.InetAddress;
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cz.zcu.kiv.fav.overlaynetwork.membership.Neighborhood;
import cz.zcu.kiv.fav.overlaynetwork.membership.Node;

/**
 * @author Lukáš Kvídera
 */
public class ExchangeServer extends UnicastRemoteObject implements NeighborhoodExchange {

  private static final long serialVersionUID = 810342758548642619L;
  private static final Logger LOG = LoggerFactory.getLogger(ExchangeServer.class);

  public ExchangeServer(final String hostname) throws RemoteException, MalformedURLException {
    Naming.rebind(getURL(hostname, "overlay-exchange-service"), this);
  }

  @Override
  public Neighborhood getNeighbors(final InetAddress who) {
    LOG.trace("getNeighbors(who={})", who);
    final Neighborhood neighbors = Node.THIS_INSTANCE.getNeighbors();
    if (!Node.THIS_INSTANCE.getHostName().equals(who.getHostName())) {
      LOG.debug("this={}, other={}", Node.THIS_INSTANCE.getHostName(), who.getHostName());
      neighbors.add(who);
    }
    return neighbors;
  }
}
