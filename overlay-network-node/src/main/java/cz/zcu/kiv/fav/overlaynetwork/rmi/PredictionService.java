/**
 * Prediction.java
 * 5. 3. 2016
 */
package cz.zcu.kiv.fav.overlaynetwork.rmi;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

import cz.zcu.kiv.fav.overlaynetwork.prediction.LinkStatePrediction;

/**
 * @author Lukáš Kvídera
 */
public interface PredictionService extends Remote {

  List<LinkStatePrediction> getLinkStatePrediction() throws RemoteException;

}
