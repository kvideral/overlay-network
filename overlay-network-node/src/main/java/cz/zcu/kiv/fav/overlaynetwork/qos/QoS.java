/**
 * QoS.java
 * 19. 4. 2016
 */
package cz.zcu.kiv.fav.overlaynetwork.qos;


/**
 * @author Lukáš Kvídera
 */
public enum QoS {
  DELAY,
  THROUGHPUT
}
