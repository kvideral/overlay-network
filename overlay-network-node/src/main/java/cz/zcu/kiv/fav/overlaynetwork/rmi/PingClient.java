/**
 * PiungClient.java
 * 13. 3. 2016
 */
package cz.zcu.kiv.fav.overlaynetwork.rmi;

import static cz.zcu.kiv.fav.overlaynetwork.utils.RmiNameUtils.getURL;

import java.net.InetAddress;
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

/**
 * @author Lukáš Kvídera
 */
public class PingClient {

  private final PingService pinger;

  public PingClient(final InetAddress forwarder) throws MalformedURLException, RemoteException, NotBoundException {
    this.pinger = (PingService) Naming.lookup(
        getURL(forwarder.getHostAddress(), "overlay-ping-service"));
  }

  public long ping(final InetAddress destination) throws RemoteException {
    return this.pinger.ping(destination);
  }
}
