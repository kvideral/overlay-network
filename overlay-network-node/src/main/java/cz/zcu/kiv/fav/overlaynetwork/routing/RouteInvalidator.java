/**
 * RouteInvalidator.java
 * 13. 3. 2016
 */
package cz.zcu.kiv.fav.overlaynetwork.routing;

import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cz.zcu.kiv.fav.overlaynetwork.qos.QoS;
import cz.zcu.kiv.fav.overlaynetwork.utils.InetAddressReader;

/**
 * @author Lukáš Kvídera
 */
public class RouteInvalidator {

  private static final Logger LOG = LoggerFactory.getLogger(RouteInvalidator.class);

  /* Prevent GC */
  @SuppressWarnings("unused")
  private static RouteInvalidator INSTANCE;

  private final ServerSocket socket;

  private RouteInvalidator() throws UnknownHostException, IOException {
    this.socket = new ServerSocket(1084, 100, InetAddress.getLoopbackAddress());

    final Thread routeInvalidator = new Thread(() -> this.discardRoutes(), this.getClass().getSimpleName());
    routeInvalidator.setDaemon(true);
    routeInvalidator.start();
  }

  public static void init() throws UnknownHostException, IOException {
    INSTANCE = new RouteInvalidator();
  }

  private void discardRoutes() {
    while (true) {
      try (final Socket connected = this.socket.accept()) {
        final InetAddress address = new InetAddressReader(connected.getInputStream()).read();
        for (final QoS type : QoS.values()) {
          RoutingTable.getInstance(type).removeRoute(address);
        }
      } catch (final IOException e) {
        LOG.error("Cannot discard route!", e);
      }
    }
  }
}
