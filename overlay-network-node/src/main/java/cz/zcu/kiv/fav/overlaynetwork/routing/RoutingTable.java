/**
 * RoutingTable.java
 * 9. 3. 2016
 */
package cz.zcu.kiv.fav.overlaynetwork.routing;

import java.net.InetAddress;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cz.zcu.kiv.fav.overlaynetwork.qos.QoS;

/**
 * @author Lukáš Kvídera
 */
public class RoutingTable {

  private static final Logger LOG = LoggerFactory.getLogger(RoutingTable.class);
  private static final RoutingTable DELAY_INSTANCE = new RoutingTable();
  private static final RoutingTable THROUGHPUT_INSTANCE = new RoutingTable();

  private final Map<InetAddress, Route> table = Collections.synchronizedMap(new HashMap<>());

  public static final class Route {
    public final long timestamp = System.currentTimeMillis();
    public final InetAddress destination;
    public final InetAddress nextHop;

    Route(final InetAddress destination, final InetAddress nextHop) {
      this.destination = destination;
      this.nextHop = nextHop;
    }

    @Override
    public String toString() {
      return "Route [timestamp=" + this.timestamp + ", destination=" + this.destination + ", nextHop=" + this.nextHop
          + "]";
    }

  }

  private RoutingTable() {

  }


  public void addRoute(final InetAddress destination, final InetAddress nextHop) {
    LOG.trace("addRoute(destination, nextHop={})", destination, nextHop);
    this.table.put(destination, new Route(destination, nextHop));
  }


  /**
   * @param byAddress
   */
  public void removeRoute(final InetAddress byAddress) {
    LOG.trace("removeRoute(byAddress={})", byAddress);
    this.table.remove(byAddress);
  }


  public static RoutingTable getDelayInstance() {
    return DELAY_INSTANCE;
  }


  public static RoutingTable getThrouhgputInstance() {
    return THROUGHPUT_INSTANCE;
  }


  public Collection<Route> getRoutes() {
    return this.table.values();
  }



  @Override
  public String toString() {
    final StringBuilder sb = new StringBuilder(this.table.size() * Long.BYTES);
    this.table.forEach((source, nextHop) -> sb.append('\n').append(source).append(" -> ").append(nextHop));
    return sb.toString();
  }


  /**
   * @param type
   * @return
   */
  public static RoutingTable getInstance(final QoS type) {
    switch (type) {
      case DELAY:
        return getDelayInstance();

      case THROUGHPUT:
        return getThrouhgputInstance();
    }

    return null;
  }

}
