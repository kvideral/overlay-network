/**
 * RmiNameUtils.java
 * 6. 3. 2016
 */
package cz.zcu.kiv.fav.overlaynetwork.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Lukáš Kvídera
 */
public final class RmiNameUtils {

  private static final Logger LOG = LoggerFactory.getLogger(RmiNameUtils.class);

  private RmiNameUtils() {
    /* hidden */
  }

  public static String getURL(final String host, final String service) {
    LOG.trace("getURL(host={})", host);
    return "//" + host + "/" + service;
  }
}
