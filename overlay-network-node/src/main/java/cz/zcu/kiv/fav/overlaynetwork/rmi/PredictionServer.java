/**
 * PredictionServer.java
 * 6. 3. 2016
 */
package cz.zcu.kiv.fav.overlaynetwork.rmi;

import static cz.zcu.kiv.fav.overlaynetwork.utils.RmiNameUtils.getURL;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.sql.SQLException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cz.zcu.kiv.fav.overlaynetwork.membership.Neighborhood;
import cz.zcu.kiv.fav.overlaynetwork.membership.Node;
import cz.zcu.kiv.fav.overlaynetwork.membership.StatisticalDataDao;
import cz.zcu.kiv.fav.overlaynetwork.prediction.LinkStatePrediction;
import cz.zcu.kiv.fav.overlaynetwork.prediction.PredictionEngine;

/**
 * @author Lukáš Kvídera
 */
public class PredictionServer extends UnicastRemoteObject implements PredictionService {

  private static final long serialVersionUID = -2275161778928778576L;

  private static final Logger LOG = LoggerFactory.getLogger(PredictionServer.class);

  private transient PredictionEngine engine;

  public PredictionServer(final String hostname) throws RemoteException, MalformedURLException {
    try {
      this.engine = PredictionEngine.getArimaInstance(new StatisticalDataDao());
      Naming.rebind(getURL(hostname, "overlay-prediction-service"), this);
    } catch (final SQLException e) {
      LOG.error("Cannot create prediction server!", e);
    }
  }

  @Override
  public List<LinkStatePrediction> getLinkStatePrediction() throws RemoteException {
    LOG.trace("getLinkStatePrediction()");
    final Neighborhood neighbors = Node.THIS_INSTANCE.getNeighbors();
    final List<LinkStatePrediction> prediction = this.engine.getLinkStatePrediction(neighbors.stream());
    return prediction;
  }

}
