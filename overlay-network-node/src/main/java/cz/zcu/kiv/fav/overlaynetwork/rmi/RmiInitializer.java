/**
 * RmiInitializer.java
 * 6. 3. 2016
 */
package cz.zcu.kiv.fav.overlaynetwork.rmi;

import java.net.MalformedURLException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.ArrayList;

/**
 * @author Lukáš Kvídera
 */
public class RmiInitializer {

  private static java.util.List<Object> referenceHolder = new ArrayList<>(3);

  public static void init() throws RemoteException, MalformedURLException {
    final String hostname = System.getProperty("thisAddress", "localhost");
    System.setProperty("java.rmi.server.hostname", hostname);
    LocateRegistry.createRegistry(Registry.REGISTRY_PORT);
    referenceHolder.add(new ExchangeServer(hostname));
    referenceHolder.add(new PredictionServer(hostname));
    referenceHolder.add(new PingServer(hostname));
  }
}
