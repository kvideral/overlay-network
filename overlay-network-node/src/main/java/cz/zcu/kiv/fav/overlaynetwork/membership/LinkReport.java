/**
 * LinkReport.java
 * 11. 3. 2016
 */
package cz.zcu.kiv.fav.overlaynetwork.membership;

import java.net.InetAddress;
import java.util.ArrayList;
import java.util.List;
import java.util.function.LongSupplier;

/**
 * This class contains time serie based data of link determined by {@link InetAddress}.
 *
 * @author Lukáš Kvídera
 */
public class LinkReport {

  public static class Delay implements LongSupplier {

    private final long delay;

    public Delay(final long delay) {
      this.delay = delay;
    }

    @Override
    public long getAsLong() {
      return this.delay;
    }

    @Override
    public String toString() {
      return String.valueOf(this.delay);
    }

  }

  public static class Throughput implements LongSupplier {

    private final long throughput;

    public Throughput(final long throughput) {
      this.throughput = throughput;
    }

    @Override
    public long getAsLong() {
      return this.throughput;
    }

    @Override
    public String toString() {
      return String.valueOf(this.throughput);
    }
  }


  private final InetAddress address;
  private final List<Delay> delayTimeSerie = new ArrayList<>();
  private final List<Delay> delayHourlyTimeSerie = new ArrayList<>();
  private final List<Throughput> throughputTimeSerie = new ArrayList<>();
  private final List<Throughput> throughputHourlyTimeSerie = new ArrayList<>();


  public LinkReport(final InetAddress address) {
    this.address = address;
  }

  public InetAddress getAddress() {
    return this.address;
  }

  public List<Delay> getDelayTimeSerie() {
    return this.delayTimeSerie;
  }

  public List<Delay> getDelayHourlyTimeSerie() {
    return this.delayHourlyTimeSerie;
  }

  public List<Throughput> getThroughputTimeSerie() {
    return this.throughputTimeSerie;
  }

  public List<Throughput> getThroughputHourlyTimeSerie() {
    return this.throughputHourlyTimeSerie;
  }

  public void add(final Delay delay) {
    this.delayTimeSerie.add(delay);
  }

  public void addHourly(final Delay delay) {
    this.delayHourlyTimeSerie.add(delay);
  }

  public void add(final Throughput throughput) {
    this.throughputTimeSerie.add(throughput);
  }

  public void addHourly(final Throughput throughput) {
    this.throughputHourlyTimeSerie.add(throughput);
  }

}

