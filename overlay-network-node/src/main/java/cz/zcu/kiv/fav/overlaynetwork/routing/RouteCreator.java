/**
 * RouteCreator.java
 * 13. 3. 2016
 */
package cz.zcu.kiv.fav.overlaynetwork.routing;

import java.io.DataInputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.rmi.NotBoundException;
import java.sql.SQLException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cz.zcu.kiv.fav.overlaynetwork.graph.Edge;
import cz.zcu.kiv.fav.overlaynetwork.graph.Graph;
import cz.zcu.kiv.fav.overlaynetwork.graph.GraphBuilder;
import cz.zcu.kiv.fav.overlaynetwork.graph.Vertex;
import cz.zcu.kiv.fav.overlaynetwork.measurement.PingServiceImpl;
import cz.zcu.kiv.fav.overlaynetwork.membership.Node;
import cz.zcu.kiv.fav.overlaynetwork.membership.StatisticalDataDao;
import cz.zcu.kiv.fav.overlaynetwork.prediction.LinkStatePrediction;
import cz.zcu.kiv.fav.overlaynetwork.prediction.PredictionEngine;
import cz.zcu.kiv.fav.overlaynetwork.qos.QoSClassifier;
import cz.zcu.kiv.fav.overlaynetwork.rmi.PingClient;
import cz.zcu.kiv.fav.overlaynetwork.rmi.PingService;
import cz.zcu.kiv.fav.overlaynetwork.utils.InetAddressReader;
import cz.zcu.kiv.fav.overlaynetwork.utils.InetAddressWriter;

/**
 * @author Lukáš Kvídera
 */
public class RouteCreator {

  private static final Logger LOG = LoggerFactory.getLogger(RouteCreator.class);

  /* Prevent GC */
  @SuppressWarnings("unused")
  private static RouteCreator INSTANCE;

  private final ServerSocket socket;
  private final ExecutorService executor = Executors.newSingleThreadExecutor();
  private final PredictionEngine prediction;


  private RouteCreator() throws UnknownHostException, IOException, SQLException {
    this.socket = new ServerSocket(1083, 100, InetAddress.getLoopbackAddress());
    this.prediction = PredictionEngine.getArimaInstance(new StatisticalDataDao());

    final Thread routeInvalidator = new Thread(() -> this.acceptRouteRequest(), this.getClass().getSimpleName());
    routeInvalidator.setDaemon(true);
    routeInvalidator.start();
  }


  public static void init() throws UnknownHostException, IOException, SQLException {
    INSTANCE = new RouteCreator();
  }


  private void acceptRouteRequest() {
    while (true) {
      try {
        @SuppressWarnings("resource")
        final Socket client = this.socket.accept();
        final InetAddress destination = new InetAddressReader(client.getInputStream()).read();
        final short port = new DataInputStream(client.getInputStream()).readShort();
        this.executor.execute(() -> {
            switch (QoSClassifier.classify(port)) {
              case DELAY:
                this.findRouteByDelay(client, destination);
                break;

              case THROUGHPUT:
                this.findRouteByThroughput(client, destination);
                break;

              default:
                LOG.warn("Unknown QoS class!");
                try {
                  client.close();
                } catch (final Exception e) {
                  LOG.error("Cannot close socket!", e);
                }
                break;
            }
          }
        );
      } catch (final Exception e) {
        LOG.error("Bad socket.", e);
      }
    }
  }

  /**
   * This method measures delay from local point of view and also from view of other nodes.
   *
   * From measured delays is constructed graph and the shortest path is determined by Dijkstra's algorithm.
   *
   * @param client
   * @param costSupplier
   */
  private void findRouteByDelay(final Socket client, final InetAddress destination) {
    LOG.trace("findRouteByDelay(client)");
    try (final Socket socket = client; final PingService pinger = new PingServiceImpl()) {
      final Graph<InetAddress> graph = new Graph<>(InetAddress.getLocalHost());
      final Vertex<InetAddress> thisNode = graph.getUniqueVertex(InetAddress.getLocalHost());
      final Vertex<InetAddress> destinationVertex = graph.getUniqueVertex(destination);
      thisNode.add(new Edge<>(pinger.ping(destination), destinationVertex));

      new GraphBuilder(graph, this.prediction).buildGraph(LinkStatePrediction::getDelay);
      for (final InetAddress forwarder : Node.THIS_INSTANCE.getNeighbors()) {
        try {
          final PingClient remotePinger = new PingClient(forwarder);
          final Vertex<InetAddress> hop = graph.getUniqueVertex(forwarder);
          final long delay = remotePinger.ping(destination);
          hop.add(new Edge<>(delay, destinationVertex));
        } catch (final NotBoundException e) {
          LOG.warn("Cannot measure ping from {}, {}", forwarder, e);
        }
      }

      graph.doDijkstra();
      final InetAddress nextHop = graph.findNextHop(destinationVertex).data;
      RoutingTable.getDelayInstance().addRoute(destination, nextHop);
      new InetAddressWriter(socket.getOutputStream()).write(nextHop);
    } catch (final IOException e) {
      LOG.error("Cannot send or receive route.", e);
    }
  }

 /**
  * This method finds the best throughput path inside overlay network.
  *
  * From measured delays is constructed graph and the shortest path is determined by Dijkstra's algorithm.
  *
  * @param client
  * @param costSupplier
  */
  private void findRouteByThroughput(final Socket client, final InetAddress destination) {
    LOG.trace("findRoute(client)");
    try (final Socket socket = client) {
      final Graph<InetAddress> graph = new Graph<>(InetAddress.getLoopbackAddress());
      final Vertex<InetAddress> destinationVertex = graph.getUniqueVertex(destination);

      new GraphBuilder(graph, this.prediction).buildGraph(LinkStatePrediction::getThroughput);

      // We cannot measure throughput from edge routers, so just measure delay
      for (final InetAddress forwarder : Node.THIS_INSTANCE.getNeighbors()) {
        try {
          final PingClient remotePinger = new PingClient(forwarder);
          final Vertex<InetAddress> hop = graph.getUniqueVertex(forwarder);
          final long delay = remotePinger.ping(destination);
          hop.add(new Edge<>(delay, destinationVertex));
        } catch (final NotBoundException e) {
          LOG.warn("Cannot measure ping from {}, {}", forwarder, e);
        }
      }

      graph.doDijkstra();
      final InetAddress nextHop = graph.findNextHop(destinationVertex).data;
      RoutingTable.getThrouhgputInstance().addRoute(destination, nextHop);
      new InetAddressWriter(socket.getOutputStream()).write(nextHop);
    } catch (final IOException e) {
      LOG.error("Cannot send or receive route.", e);
    }
  }
}
