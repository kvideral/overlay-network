/**
 * MembershipJob.java
 * 6. 3. 2016
 */
package cz.zcu.kiv.fav.overlaynetwork.membership;

import java.net.InetAddress;
import java.net.MalformedURLException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.Optional;

import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cz.zcu.kiv.fav.overlaynetwork.rmi.ExchangeClient;



/**
 * @author Lukáš Kvídera
 */
@DisallowConcurrentExecution
public class MembershipJob implements Job {

  private static final Logger LOG = LoggerFactory.getLogger(MembershipJob.class);

  @Override
  public void execute(final JobExecutionContext context) throws JobExecutionException {
    final Optional<InetAddress> master = Node.getMaster();
    if (!master.isPresent()) {
      LOG.warn("No master node!");
      return;
    }

    try {
      final InetAddress host = master.get();
      final ExchangeClient client = new ExchangeClient(host);
      LOG.debug("execute(host={})", host);
      final Neighborhood neighbors = client.getNeighborsFor(Node.THIS_INSTANCE.getAddress());
      Node.mergeNeighborhoods(neighbors);
    } catch (MalformedURLException | RemoteException | NotBoundException e) {
      LOG.error("Cannot comunicate with master node!", e);
    }

  }

}
