/**
 * RoutingTableUpdateJob.java
 * 6. 3. 2016
 */
package cz.zcu.kiv.fav.overlaynetwork.routing;

import java.io.IOException;

import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cz.zcu.kiv.fav.overlaynetwork.proxy.ProxyClient;
import cz.zcu.kiv.fav.overlaynetwork.qos.QoS;


/**
 * Updates routing table in proxy.
 *
 * @author Lukáš Kvídera
 */
@DisallowConcurrentExecution
public class RoutingTableUpdateJob implements Job {
  private static final Logger LOG = LoggerFactory.getLogger(RoutingTableUpdateJob.class);

  @Override
  public void execute(final JobExecutionContext context) throws JobExecutionException {
    LOG.trace("execute(context)");
    for (final QoS type : QoS.values()) {
      try (final ProxyClient proxyClient = new ProxyClient(type)) {
        proxyClient.sendRoutingTable(RoutingTable.getInstance(type));
      } catch (final IOException e) {
        LOG.error("Cannto sent routing table!", e);
      }
    }
  }
}
