/**
 * PingServer.java
 * 13. 3. 2016
 */
package cz.zcu.kiv.fav.overlaynetwork.rmi;

import static cz.zcu.kiv.fav.overlaynetwork.utils.RmiNameUtils.getURL;

import java.io.IOException;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cz.zcu.kiv.fav.overlaynetwork.measurement.PingServiceImpl;

/**
 * @author Lukáš Kvídera
 */
public class PingServer extends UnicastRemoteObject implements PingService {

  private static final Logger LOG = LoggerFactory.getLogger(PingServer.class);

  private static final long serialVersionUID = 3376327835449286956L;

  public PingServer(final String hostname) throws RemoteException, MalformedURLException {
    Naming.rebind(getURL(hostname, "overlay-ping-service"), this);
  }

  @Override
  public long ping(final InetAddress destination) throws RemoteException {
    LOG.trace("ping(destination={})", destination);
    try (final PingService pinger = new PingServiceImpl()) {
      return pinger.ping(destination);
    } catch (final Exception e) {
      throw new RemoteException("Cannot ping", e);
    }
  }

  @Override
  public void close() throws IOException {
    /* do nothing */
  }

}
