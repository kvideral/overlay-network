/**
 * PingService.java
 * 13. 3. 2016
 */
package cz.zcu.kiv.fav.overlaynetwork.measurement;

import static cz.zcu.kiv.fav.overlaynetwork.utils.InetConstants.SOCKADDR_STORAGE_SIZE;
import static cz.zcu.kiv.fav.overlaynetwork.utils.InetConstants.getFamily;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.nio.ByteBuffer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cz.zcu.kiv.fav.overlaynetwork.rmi.PingService;

/**
 * @author Lukáš Kvídera
 */
public class PingServiceImpl implements PingService {

  private static final Logger LOG = LoggerFactory.getLogger(PingServiceImpl.class);

  private final DatagramSocket socket;
  private final byte[] header = new byte[SOCKADDR_STORAGE_SIZE];

  public PingServiceImpl() throws SocketException {
    this.socket = new DatagramSocket();
    this.socket.setSoTimeout(2000);
  }

  @Override
  public long ping(final InetAddress destination) {
    LOG.trace("ping(destination={})", destination);
    final ByteBuffer buffer = ByteBuffer.allocate(this.header.length);
    final byte[] message = packMessage(buffer, destination);
    final DatagramPacket ping = new DatagramPacket(message, message.length, InetAddress.getLoopbackAddress(), 1082);

    try {
      this.socket.send(ping);
      this.socket.receive(ping);
    } catch (final IOException e) {
      LOG.error("Cannot receive ping!", e);
      return Long.MAX_VALUE;
    }

    final byte[] data = ping.getData();
    final long delay = ByteBuffer.wrap(data).getLong();
    return delay;
  }

  private static byte[] packMessage(final ByteBuffer buffer, final InetAddress address) {
    return buffer.putInt(getFamily(address)).put(address.getAddress()).array();
  }

  @Override
  public void close() throws IOException {
    this.socket.close();
  }
}
