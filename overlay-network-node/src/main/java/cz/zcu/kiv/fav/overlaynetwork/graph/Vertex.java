/**
 * Vertex.java
 * 6. 3. 2016
 */
package cz.zcu.kiv.fav.overlaynetwork.graph;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Lukáš Kvídera
 */
public class Vertex<T> implements Nameable, Comparable<Vertex<T>> {

  private static final Logger LOG = LoggerFactory.getLogger(Vertex.class);

  public final T data;
  private long distance = Long.MAX_VALUE;
  private boolean processed;
  private Vertex<T> previous = this;
  private final List<Edge<Vertex<T>>> links = new ArrayList<>();


  public Vertex(final T data) {
    this.data = data;
  }


  public boolean add(final Edge<Vertex<T>> e) {
    return this.links.add(e);
  }

  public List<Edge<Vertex<T>>> getEdges() {
    return Collections.unmodifiableList(this.links);
  }

  public long getDistance() {
    return this.distance;
  }


  public boolean isProcessed() {
    return this.processed;
  }

  public void setProcessed() {
    this.processed = true;
  }

  public Vertex<T> getPrevious() {
    return this.previous;
  }

  public void updateDistance(final Edge<Vertex<T>> edge, final Vertex<T> previous) {
    final long newDistance = this.distance == Long.MAX_VALUE ? edge.cost : edge.cost + previous.distance;
    if (newDistance < 0) {
      LOG.warn("Integer overflow!");
      return;
    }

    if (newDistance < this.distance) {
      this.distance = newDistance;
      this.previous = previous;
    }
  }

  @Override
  public String toString() {
    return "Vertex [\n\tdata=" + this.data
        + "\n\tdistance=" + this.distance
        + "\n\tprocessed=" + this.processed
        + "\n\tthis.links=\n\t" + this.links.stream()
        .map(link -> link.toString())
        .collect(Collectors.joining("\n\t")) + "]";
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((this.data == null) ? 0 : this.data.hashCode());
    return result;
  }

  @Override
  public boolean equals(final Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (this.getClass() != obj.getClass()) {
      return false;
    }
    final Vertex<?> other = (Vertex<?>) obj;
    if (this.data == null) {
      if (other.data != null) {
        return false;
      }
    } else if (!this.data.equals(other.data)) {
      return false;
    }
    return true;
  }


  @Override
  public String getName() {
    return this.data.toString();
  }


  @Override
  public int compareTo(final Vertex<T> o) {
    final long diff = this.distance - o.distance;

    if (diff > 0) {
      return 1;
    } else if (diff < 0) {
      return -1;
    }

    return 0;
  }

}
