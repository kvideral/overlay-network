package cz.zcu.kiv.fav.overlaynetwork;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.UnknownHostException;
import java.rmi.RemoteException;
import java.sql.SQLException;

import org.quartz.Job;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SimpleScheduleBuilder;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;

import cz.zcu.kiv.fav.overlaynetwork.config.Configuration;
import cz.zcu.kiv.fav.overlaynetwork.config.GlobalConfigurationLoader;
import cz.zcu.kiv.fav.overlaynetwork.measurement.DelayMeasurementJob;
import cz.zcu.kiv.fav.overlaynetwork.measurement.HourlyAgregationJob;
import cz.zcu.kiv.fav.overlaynetwork.measurement.ThroughputMeasurementJob;
import cz.zcu.kiv.fav.overlaynetwork.membership.MembershipJob;
import cz.zcu.kiv.fav.overlaynetwork.rmi.RmiInitializer;
import cz.zcu.kiv.fav.overlaynetwork.routing.RouteCreator;
import cz.zcu.kiv.fav.overlaynetwork.routing.RouteInvalidator;
import cz.zcu.kiv.fav.overlaynetwork.routing.RouteMarkToRefreshJob;
import cz.zcu.kiv.fav.overlaynetwork.routing.RoutingTableUpdateJob;

/**
 * -DthisAddress=<adresa toholde uzlu>
 * -DmasterAddress=<volitelná adresa souseda>, pokud není zadána, vezme se náhodný sosed
 * @author Lukáš Kvídera
 */
public class Main {

  private  static <T extends Job> JobDetail buildJob(final Class<T> clazz, final String key, final Integer value) {
    return JobBuilder.newJob(clazz)
        .withIdentity(clazz.getName(), clazz.getCanonicalName())
        .usingJobData(key, value)
        .build();
  }

  private  static <T extends Job> JobDetail buildJob(final Class<T> clazz) {
    return JobBuilder.newJob(clazz)
        .withIdentity(clazz.getName(), clazz.getCanonicalName())
        .build();
  }

  private static Trigger buildTrigger(final int delay) {
    return TriggerBuilder
        .newTrigger()
        .withSchedule(
            SimpleScheduleBuilder.simpleSchedule()
            .withIntervalInSeconds(delay)
            .repeatForever())
        .build();
  }

  private static void initializePing(final Scheduler scheduler, final Configuration configuration)
      throws SchedulerException {
    final JobDetail pingJob = buildJob(DelayMeasurementJob.class);
    final Trigger trigger = buildTrigger(configuration.getPingInterval());
    scheduler.scheduleJob(pingJob, trigger);
  }


  private static void initializeAgregationJob(final Scheduler scheduler, final Configuration configuration)
      throws SchedulerException {
    final JobDetail pingJob = buildJob(HourlyAgregationJob.class);
    final Trigger trigger = buildTrigger(24);
    scheduler.scheduleJob(pingJob, trigger);
  }

  private static void initializeThroughput(final Scheduler scheduler, final Configuration configuration)
      throws SchedulerException {
    final JobDetail pingJob = buildJob(ThroughputMeasurementJob.class);
    final Trigger trigger = buildTrigger(120);
    scheduler.scheduleJob(pingJob, trigger);
  }

  /**
   * @param scheduler
   * @param configuration
   * @throws SchedulerException
   */
  private static void initializeMembership(final Scheduler scheduler, final Configuration configuration) throws SchedulerException {
    final JobDetail membershipJob = buildJob(MembershipJob.class);
    final Trigger trigger = buildTrigger(10);
    scheduler.scheduleJob(membershipJob, trigger);
  }

//  private static void initializeDijkstra(final Scheduler scheduler, final Configuration configuration)
//      throws SchedulerException {
//    final JobDetail dijkstraJob = buildJob(PredictedDijkstraJob.class);
//    final Trigger trigger = buildTrigger(configuration.getRouteCalculationInterval());
//    scheduler.scheduleJob(dijkstraJob, trigger);
//  }

  private static void initializeRoutingTableUpdate(final Scheduler scheduler, final Configuration configuration)
      throws SchedulerException {
    final JobDetail routingTableJob = buildJob(RoutingTableUpdateJob.class);
    final Trigger trigger = buildTrigger(configuration.getRouteUpdateInterval());
    scheduler.scheduleJob(routingTableJob, trigger);
  }

  private static void initializeRoutingTableRefresh(final Scheduler scheduler, final Configuration configuration)
      throws SchedulerException {
    final JobDetail routingTableJob = buildJob(RouteMarkToRefreshJob.class,
        "routeTTL", Integer.valueOf(configuration.getRouteTTL()));
    final Trigger trigger = buildTrigger(configuration.getRouteUpdateInterval());
    scheduler.scheduleJob(routingTableJob, trigger);
  }

  private static void runRmiService() throws RemoteException, MalformedURLException {
    RmiInitializer.init();
  }

  private static void runRouteManipulation() throws UnknownHostException, IOException, SQLException {
    RouteCreator.init();
    RouteInvalidator.init();
  }

  public static void main(final String[] args)
      throws SchedulerException, UnknownHostException, IOException, SQLException {
    final Configuration configuration = GlobalConfigurationLoader.getConfiguration();


    final Scheduler scheduler = new StdSchedulerFactory().getScheduler();
    scheduler.start();

    initializePing(scheduler, configuration);
    initializeAgregationJob(scheduler, configuration);
    initializeThroughput(scheduler, configuration);
    initializeMembership(scheduler, configuration);
//    initializeDijkstra(scheduler, configuration);
    initializeRoutingTableUpdate(scheduler, configuration);
    initializeRoutingTableRefresh(scheduler, configuration);
    runRmiService();
    runRouteManipulation();
  }

}
