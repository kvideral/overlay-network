/**
 * QoSClassifier.java
 * 19. 4. 2016
 */
package cz.zcu.kiv.fav.overlaynetwork.qos;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Lukáš Kvídera
 */
public class QoSClassifier {

  private static final Logger LOG = LoggerFactory.getLogger(QoSClassifier.class);
  private static final Map<Short, QoS> classes;

  static {
    final Map<Short, QoS> t = new HashMap<>();
    t.put(Short.valueOf("80"), QoS.DELAY);
    t.put(Short.valueOf("8080"), QoS.THROUGHPUT);

    classes = Collections.unmodifiableMap(t);
  }

  public static QoS classify(final short port) {
    LOG.trace("classify(port={})", port);
    return classes.getOrDefault(Short.valueOf(port), QoS.DELAY);
  }
}
