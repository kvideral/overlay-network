/**
 * ArimaCalculator.java
 * 19. 3. 2016
 */
package cz.zcu.kiv.fav.overlaynetwork.prediction;

import java.util.List;
import java.util.function.LongSupplier;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Lukáš Kvídera
 */
public class DummyCalculator implements PredictionCalculator {

  private static final Logger LOG = LoggerFactory.getLogger(DummyCalculator.class);

  @Override
  public Prediction calculate(final List<? extends LongSupplier> data) {
    LOG.trace("calculate(data)");
    if (data.isEmpty()) {
      return () -> Integer.MAX_VALUE;
    }
    return () -> data.get(data.size() - 1).getAsLong();
  }
}
