/**
 * RoutingTableUpdateJob.java
 * 6. 3. 2016
 */
package cz.zcu.kiv.fav.overlaynetwork.routing;

import java.util.Iterator;

import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cz.zcu.kiv.fav.overlaynetwork.qos.QoS;
import cz.zcu.kiv.fav.overlaynetwork.routing.RoutingTable.Route;


/**
 * Deletes old routes. {@link RouteCreator} will create a new route based on latest prediction.
 *
 * @author Lukáš Kvídera
 */
@DisallowConcurrentExecution
public class RouteMarkToRefreshJob implements Job {
  private static final Logger LOG = LoggerFactory.getLogger(RouteMarkToRefreshJob.class);

  @Override
  public void execute(final JobExecutionContext context) throws JobExecutionException {
    LOG.trace("execute(context)");
    for (final QoS type : QoS.values()) {
      final RoutingTable routingTable = RoutingTable.getInstance(type);
      final int LIFE_TIME = ((Integer) context
          .getJobDetail().getJobDataMap().getOrDefault("routeTTL", Integer.valueOf(10000))).intValue();

      for (final Iterator<Route> iterator = routingTable.getRoutes().iterator(); iterator.hasNext();) {
        final Route route = iterator.next();
        if (route.timestamp + LIFE_TIME < System.currentTimeMillis()) {
          iterator.remove();
        }
      }
    }
  }
}
