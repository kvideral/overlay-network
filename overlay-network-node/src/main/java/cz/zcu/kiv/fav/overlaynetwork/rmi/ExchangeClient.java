/**
 * ExchangeClient.java
 * 5. 3. 2016
 */
package cz.zcu.kiv.fav.overlaynetwork.rmi;

import static cz.zcu.kiv.fav.overlaynetwork.utils.RmiNameUtils.getURL;

import java.net.InetAddress;
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cz.zcu.kiv.fav.overlaynetwork.membership.Neighborhood;
import cz.zcu.kiv.fav.overlaynetwork.prediction.LinkStatePrediction;

/**
 * @author Lukáš Kvídera
 */
public class ExchangeClient {

  private static final Logger LOG = LoggerFactory.getLogger(ExchangeClient.class);

  private final NeighborhoodExchange neighborOfNode;
  private final PredictionService predictionService;

  public ExchangeClient(final InetAddress destination) throws MalformedURLException, RemoteException, NotBoundException {
    this.neighborOfNode = (NeighborhoodExchange) Naming.lookup(
        getURL(destination.getHostAddress(), "overlay-exchange-service"));
    this.predictionService = (PredictionService) Naming.lookup(
        getURL(destination.getHostAddress(), "overlay-prediction-service"));
  }

  /**
   * Retrieves neighbors without source.
   * Split horizon.
   * @param source
   * @return
   * @throws MalformedURLException
   * @throws RemoteException
   * @throws NotBoundException
   */
  public Neighborhood getNeighborsFor(final InetAddress source) throws RemoteException {
    LOG.trace("getNeighborsOf(address={})", source);
    final Neighborhood neighbors = this.neighborOfNode.getNeighbors(source);
    return neighbors;
  }

  public List<LinkStatePrediction> getLinkStatePrediction() throws RemoteException {
    return this.predictionService.getLinkStatePrediction();
  }

}
