/**
 * Neighborhood.java
 * 5. 3. 2016
 */
package cz.zcu.kiv.fav.overlaynetwork.membership;

import java.io.Serializable;
import java.net.InetAddress;
import java.util.Collections;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Lukáš Kvídera
 */
public class Neighborhood implements Iterable<InetAddress>, Serializable {

  private static final Logger LOG = LoggerFactory.getLogger(Neighborhood.class);

  private static final long serialVersionUID = -3662815353194152564L;

  private final Set<InetAddress> neighbors = Collections.newSetFromMap(new ConcurrentHashMap<>(1000, 0.7f, 1));

  public void add(final InetAddress neighborsAddress) {
    LOG.trace("add(neighborsAddress={})", neighborsAddress);
    this.neighbors.add(neighborsAddress);
  }

  public void add(final Neighborhood neighborhood) {
    LOG.trace("add(neighborhood={})", neighborhood);
    this.neighbors.addAll(neighborhood.neighbors);
  }

  @Override
  public String toString() {
    return "Neighborhood [neighbors=" + this.neighbors + "]";
  }

  @Override
  public Iterator<InetAddress> iterator() {
    LOG.trace("iterator()");
    return this.neighbors.iterator();
  }

  public int size() {
    return this.neighbors.size();
  }

  public void remove(final InetAddress source) {
    this.neighbors.remove(source);
  }

  public Stream<InetAddress> stream() {
    return this.neighbors.stream();
  }
}
