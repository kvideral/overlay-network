/**
 * Graph.java
 * 13. 3. 2016
 */
package cz.zcu.kiv.fav.overlaynetwork.graph;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.PriorityQueue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Lukáš Kvídera
 */
public class Graph<T> {
  private static final Logger LOG = LoggerFactory.getLogger(Graph.class);
  private final Map<T, Vertex<T>> nodes = new HashMap<>();
  private final Vertex<T> source;


  public Graph(final T source) {
    this.source = new Vertex<>(source);
    this.getSource().updateDistance(new Edge(0, this.getSource()), this.getSource());
    this.nodes.put(this.getSource().data, this.getSource());
  }


  public void doDijkstra() {
    LOG.trace("doDijkstra()");
    final PriorityQueue<Vertex<T>> queue = new PriorityQueue<>();
    queue.add(this.getSource());

    while (!queue.isEmpty()) {
      final Vertex<T> node = queue.remove();
      node.setProcessed();

      for (final Edge<Vertex<T>> edge : node.getEdges()) {
        final Vertex<T> other = edge.vertex;
        if (other.isProcessed()) {
          continue;
        }

        other.updateDistance(edge, node);
        queue.add(other);
      }
    }

    this.nodes.forEach((key, value) -> LOG.debug("{}", value));
  }


  public Vertex<T> getUniqueVertex(final T value) {
    return this.nodes.computeIfAbsent(value, key -> new Vertex<>(value));
  }


  public Vertex<T> findNextHop(Vertex<T> destination) {
    LOG.trace("findNextHop(destination={})", destination);
    Vertex<T> nextHop = destination;
    do {
      nextHop = destination;
      destination = destination.getPrevious();
    } while (destination.getPrevious() != destination);
    return nextHop;
  }


  public Vertex<T> getSource() {
    return this.source;
  }


  public Collection<Vertex<T>> values() {
    return this.nodes.values();
  }


  @Override
  public String toString() {
    return "Graph [nodes=" + this.nodes + ", source=" + this.source + "]";
  }


}
