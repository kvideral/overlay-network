/**
 * InetSocketWriter.java
 * 13. 3. 2016
 */
package cz.zcu.kiv.fav.overlaynetwork.utils;

import static cz.zcu.kiv.fav.overlaynetwork.utils.InetConstants.getFamily;

import java.io.IOException;
import java.io.OutputStream;
import java.net.InetAddress;
import java.nio.ByteBuffer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Lukáš Kvídera
 */
public class InetAddressWriter {
  private static final Logger LOG = LoggerFactory.getLogger(InetAddressWriter.class);

  private final OutputStream os;

  public InetAddressWriter(final OutputStream os) {
    this.os = os;
  }

  public void write(final InetAddress address) throws IOException {
    LOG.trace("writeAddress(address={})", address);
    final byte[] message = packMessage(address);
    this.os.write(message);
  }

  private static byte[] packMessage(final InetAddress address) {
    final int addressLength = InetConstants.getAddressLength(address);
    final ByteBuffer buffer = ByteBuffer.allocate(addressLength + Short.BYTES);
    return buffer.putShort(getFamily(address)).put(address.getAddress()).array();
  }
}
