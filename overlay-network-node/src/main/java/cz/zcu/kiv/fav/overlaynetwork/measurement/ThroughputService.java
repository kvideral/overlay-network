/**
 * ThroughputService.java
 * 23. 3. 2016
 */
package cz.zcu.kiv.fav.overlaynetwork.measurement;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sun.misc.IOUtils;

/**
 * @author Lukáš Kvídera
 */
public class ThroughputService {

  private static final Logger LOG = LoggerFactory.getLogger(ThroughputService.class);
  private static Endpoint endpoint = new Endpoint();

  private static final byte[] payload = new byte[1024 * 1024];
  public static final int PORT = 1085;


  private static class Endpoint {
    private static ExecutorService executor = Executors.newCachedThreadPool();
    private ServerSocket socket;

    public Endpoint() {
      try {
        this.socket = new ServerSocket();
        this.socket.bind(new InetSocketAddress(PORT));
        this.socket.setReceiveBufferSize(65535);
        executor.execute(this::accept);
      } catch (final IOException e) {
        LOG.error("Cannot listen for throughput.", e);
      }
    }

    private void accept() {
      while (true) {
        try {
          @SuppressWarnings("resource")
          final Socket client = this.socket.accept();
          executor.execute(() -> responseLoop(client));
        } catch (final IOException e) {
          LOG.error("Cannot accept connection!", e);
        }
      }
    }

    @SuppressWarnings("resource")
    private static void responseLoop(final Socket socket) {
      LOG.trace("responseLoop(socket)");
      try (Socket client = socket){
        final InputStream inputStream = client.getInputStream();
        IOUtils.readFully(inputStream, payload.length, true);

        final OutputStream outputStream = client.getOutputStream();
        outputStream.write(payload);
      } catch (final IOException e) {
        LOG.error("Cannot measure throughput!", e);
      }
    }
  }


  @SuppressWarnings("resource")
  public long measure(final InetAddress destination) throws IOException {
    LOG.trace("measure(destination={})", destination);
    try (final Socket socket = new Socket(destination, PORT)) {
//       socket.setTcpNoDelay(true);
      final long start = System.currentTimeMillis();
      final OutputStream outputStream = socket.getOutputStream();
      outputStream.write(payload);

      final InputStream inputStream = socket.getInputStream();
      IOUtils.readFully(inputStream, payload.length, true);

      final long rtt = System.currentTimeMillis() - start;
      return rtt == 0 ? Long.MAX_VALUE : payload.length / rtt;
    }
  }
}
