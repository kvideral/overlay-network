/**
 * ErrorReportingService.java
 * 6. 3. 2016
 */
package cz.zcu.kiv.fav.overlaynetwork.membership;

import java.net.InetAddress;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Lukáš Kvídera
 */
public class LinkStateService {

  private static final Logger LOG = LoggerFactory.getLogger(LinkStateService.class);

  private final StatisticalDataProvider dao;

  public LinkStateService(final StatisticalDataProvider dao) {
    this.dao = dao;
  }

  public void reportConnectionError(final InetAddress address) {
//    this.data.compute(address, (key, oldReport) -> {
//      oldReport = getOrNew(oldReport, address);
//      oldReport.packetloss++;
//      return oldReport;
//    });
  }

  /**
   * @param address
   * @param delay
   */
  public void reportDelay(final InetAddress address, final long delay) {
    LOG.trace("reportDelay(address={}, delay={})", address, delay);
    try {
      this.dao.persistDelay(address, delay);
    } catch (final SQLException e) {
      LOG.error("Cannot store delay!", e);
    }
  }

  /**
   * @param destination
   * @param throughput
   */
  public void reportThroughput(final InetAddress address, final long throughput) {
    LOG.trace("reportThroughput(destination={}, throughput={})", address, throughput);
    try {
      this.dao.persistThroughput(address, throughput);
    } catch (final SQLException e) {
      LOG.error("Cannot store throughput!", e);
    }
  }

}
