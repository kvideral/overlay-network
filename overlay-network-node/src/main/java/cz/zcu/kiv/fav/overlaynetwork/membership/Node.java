/**
 * Node.java
 * 5. 3. 2016
 */
package cz.zcu.kiv.fav.overlaynetwork.membership;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Objects;
import java.util.Optional;
import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Lukáš Kvídera
 */
public class Node {

  private static final Logger LOG = LoggerFactory.getLogger(Node.class);

  public static final Node THIS_INSTANCE = new Node();

  private final InetAddress address;

  /** There are managed relations between nodes */
  private Neighborhood neighbors = new Neighborhood();

  private Node() {
    InetAddress address = InetAddress.getLoopbackAddress();
    try {
      final String masterAddress = System.getProperty("masterAddress");
      final String thisAddress = System.getProperty("thisAddress");
      address = InetAddress.getByName(thisAddress);

      LOG.debug("Node(thisAddress={}, masterAddress={})", thisAddress, masterAddress);
      if (!Objects.equals(thisAddress, masterAddress)) {
        this.neighbors.add(InetAddress.getByName(masterAddress));
      }
    } catch (final UnknownHostException e) {
      LOG.error("Cannot create connection to the master!", e);
    }

    this.address = address;
  }

  public Node(final InetAddress address) {
    this.address = address;
  }

  /**
   * @return the neighbors
   */
  public Neighborhood getNeighbors() {
    return this.neighbors;
  }

  /**
   * @param neighbors the neighbors to set
   */
  public void setNeighbors(final Neighborhood neighbors) {
    this.neighbors = neighbors;
  }

  public InetAddress getAddress() {
    return this.address;
  }

  public String getHostName() {
    return this.getAddress().getHostName();
  }


  public static Optional<InetAddress> getMaster() {
    if (THIS_INSTANCE.neighbors.size() == 0) {
      return Optional.empty();
    }

    final int random = new Random().nextInt(THIS_INSTANCE.neighbors.size());
    int i = 0;
    for (final InetAddress address : THIS_INSTANCE.neighbors) {
      if (i++ == random) {
        return Optional.of(address);
      }
    }

    return Optional.empty();
  }

  public static void mergeNeighborhoods(final Neighborhood neighborhood) {
    LOG.trace("mergeNeighborhoods(neighborhood)");
    neighborhood.remove(THIS_INSTANCE.address);
    Node.THIS_INSTANCE.neighbors.add(neighborhood);
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((this.address == null) ? 0 : this.address.hashCode());
    return result;
  }

  @Override
  public boolean equals(final Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (this.getClass() != obj.getClass()) {
      return false;
    }
    final Node other = (Node) obj;
    if (this.address == null) {
      if (other.address != null) {
        return false;
      }
    } else if (!this.address.equals(other.address)) {
      return false;
    }
    return true;
  }

  @Override
  public String toString() {
    return "Node [address=" + this.address + ", neighbors=" + this.neighbors + "]";
  }
}
