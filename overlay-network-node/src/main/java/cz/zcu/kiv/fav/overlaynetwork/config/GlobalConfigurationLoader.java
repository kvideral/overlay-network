/**
 * GlobalConfiguration.java
 * 5. 3. 2016
 */
package cz.zcu.kiv.fav.overlaynetwork.config;

import static java.lang.Integer.parseInt;

import java.io.IOException;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Lukáš Kvídera
 */
public final class GlobalConfigurationLoader {

  private static final Logger LOG = LoggerFactory.getLogger(GlobalConfigurationLoader.class);

  private static Configuration configuration = Configuration.DEFAULT;

  public static Configuration getConfiguration() {
    if (configuration == Configuration.DEFAULT) {
      final Properties properties = new Properties();
      try {
        properties.load(GlobalConfigurationLoader.class
            .getClassLoader()
            .getResourceAsStream("measurement.properties")
            );

        configuration = new Configuration();
        configuration.setPingInterval(parseInt(properties.getProperty("pingInterval")));
        configuration.setRouteUpdateInterval(parseInt(properties.getProperty("routeUpdateInterval")));
        configuration.setRouteCalculationInterval(parseInt(properties.getProperty("routeCalculationInterval")));
        configuration.setRouteTTL(parseInt(properties.getProperty("routeTTL")) * 1000);
      } catch (final IOException e) {
        LOG.error("Cannot read properties!", e);
      }
    }

    return configuration;
  }
}
