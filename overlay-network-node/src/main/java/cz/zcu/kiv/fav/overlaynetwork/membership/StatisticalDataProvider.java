/**
 * StatisticalDataProvider.java
 * 11. 3. 2016
 */
package cz.zcu.kiv.fav.overlaynetwork.membership;

import java.net.InetAddress;
import java.sql.SQLException;

/**
 * @author Lukáš Kvídera
 */
public interface StatisticalDataProvider {

  void persistDelay(InetAddress address, long delay) throws SQLException;
  void persistThroughput(InetAddress address, long throughput) throws SQLException;

  LinkReport getReport(InetAddress address) throws SQLException;

}
