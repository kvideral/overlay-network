/**
 * Edge.java
 * 6. 3. 2016
 */
package cz.zcu.kiv.fav.overlaynetwork.graph;

/**
 * @author Lukáš Kvídera
 */
public class Edge<T extends Nameable> {
  public final long cost;
  public final T vertex;

  /**
   * @param delay
   * @param vertex
   */
  public Edge(final long delay, final T vertex) {
    this.cost = delay;
    this.vertex = vertex;
  }

  @Override
  public String toString() {
    return "Edge [cost=" + this.cost + "] -> " + this.vertex.getName();
  }

}
