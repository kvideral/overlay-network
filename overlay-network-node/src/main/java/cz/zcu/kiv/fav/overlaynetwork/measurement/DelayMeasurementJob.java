package cz.zcu.kiv.fav.overlaynetwork.measurement;

import java.io.IOException;
import java.net.InetAddress;
import java.sql.SQLException;

import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cz.zcu.kiv.fav.overlaynetwork.membership.LinkStateService;
import cz.zcu.kiv.fav.overlaynetwork.membership.Node;
import cz.zcu.kiv.fav.overlaynetwork.membership.StatisticalDataDao;


/**
 * DelayMeasurementJob.java
 *    5. 3. 2016
 * @author Lukáš Kvídera
 * @version 0.0
 */
@DisallowConcurrentExecution
public class DelayMeasurementJob implements Job {

  private static final Logger LOG = LoggerFactory.getLogger(DelayMeasurementJob.class);

  private final LinkStateService service;

  public DelayMeasurementJob() throws SQLException {
    this.service = new LinkStateService(new StatisticalDataDao());
  }

  @Override
  public void execute(final JobExecutionContext context) throws JobExecutionException {
    LOG.trace("execute()");
    try (final PingServiceImpl pinger = new PingServiceImpl()) {
      for (final InetAddress destination : Node.THIS_INSTANCE.getNeighbors()) {
        final long delay = pinger.ping(destination);
        LOG.info("Delay -> {} = {}", destination, delay);
        this.service.reportDelay(destination, delay);
      }
    } catch (final IOException e) {
      LOG.error("Something bad happened!", e);
    }
  }

}
