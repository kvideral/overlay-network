/**
 * UnixSocket.java
 * 6. 3. 2016
 */
package cz.zcu.kiv.fav.overlaynetwork.proxy;

import java.io.Closeable;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;
import java.net.UnknownHostException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cz.zcu.kiv.fav.overlaynetwork.qos.QoS;
import cz.zcu.kiv.fav.overlaynetwork.routing.RoutingTable;
import cz.zcu.kiv.fav.overlaynetwork.routing.RoutingTable.Route;
import cz.zcu.kiv.fav.overlaynetwork.utils.InetAddressWriter;

/**
 * @author Lukáš Kvídera
 */
public class ProxyClient implements Closeable {

  private static final Logger LOG = LoggerFactory.getLogger(ProxyClient.class);

  private final Socket socket;
  private final OutputStream os;
  private final QoS type;

  public ProxyClient(final QoS type) throws UnknownHostException, IOException {
    this.socket = new Socket("localhost", 1082);
    this.os = this.socket.getOutputStream();
    this.type = type;
  }

  public void sendRoutingTable(final RoutingTable routingTable) throws IOException {
    LOG.trace("sendRoutingTable(routingTable={})", routingTable);
    new DataOutputStream(this.os).writeByte(this.type.ordinal());
    final InetAddressWriter writer = new InetAddressWriter(this.os);
    for (final Route route : routingTable.getRoutes()) {
      writer.write(route.destination);
      writer.write(route.nextHop);
    }

  }

  @Override
  public void close() throws IOException {
    this.socket.close();
  }
}
