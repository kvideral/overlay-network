/**
 * ComplexPrediction.java
 * 6. 3. 2016
 */
package cz.zcu.kiv.fav.overlaynetwork.prediction;

import java.io.Serializable;
import java.net.InetAddress;

/**
 * @author Lukáš Kvídera
 */
public class LinkStatePrediction implements Serializable {

  private static final long serialVersionUID = 4742475398190704792L;

  private final InetAddress address;
  private final long delay;
  private final long packetloss;
  private final long throughput;


  /**
   * @param address
   * @param delay
   * @param packetloss
   * @param throughput
   */
  public LinkStatePrediction(final InetAddress address, final long delay, final long packetloss, final long throughput) {
    this.address = address;
    this.delay = delay;
    this.packetloss = packetloss;
    this.throughput = throughput;
  }

  public InetAddress getAddress() {
    return this.address;
  }

  public long getDelay() {
    return this.delay;
  }

  public long getPacketloss() {
    return this.packetloss;
  }

  public long getThroughput() {
    return this.throughput;
  }

  @Override
  public String toString() {
    return "LinkStatePrediction [address=" + this.address + ", delay=" + this.delay + ", packetloss=" + this.packetloss
        + ", throughput=" + this.throughput + "]";
  }


}
