/**
 * PredictionCalculator.java
 * 16. 3. 2016
 */
package cz.zcu.kiv.fav.overlaynetwork.prediction;

import java.util.List;
import java.util.function.LongSupplier;

/**
 * @author Lukáš Kvídera
 */
public interface PredictionCalculator {

  /**
   * @param data
   * @param dataGetter
   * @return Predicted value of calculated time series
   */
  Prediction calculate(List<? extends LongSupplier> data);

}
