/**
 * GraphBuilder.java
 * 13. 3. 2016
 */
package cz.zcu.kiv.fav.overlaynetwork.graph;

import java.net.InetAddress;
import java.net.MalformedURLException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cz.zcu.kiv.fav.overlaynetwork.membership.Node;
import cz.zcu.kiv.fav.overlaynetwork.prediction.LinkStatePrediction;
import cz.zcu.kiv.fav.overlaynetwork.prediction.PredictionCache;
import cz.zcu.kiv.fav.overlaynetwork.prediction.PredictionEngine;
import cz.zcu.kiv.fav.overlaynetwork.rmi.ExchangeClient;

/**
 * @author Lukáš Kvídera
 */
public class GraphBuilder {

  private static final Logger LOG = LoggerFactory.getLogger(GraphBuilder.class);
  private static final PredictionCache cache = new PredictionCache();

  private final Graph<InetAddress> graph;
  private final PredictionEngine prediction;


  public GraphBuilder(final Graph<InetAddress> graph, final PredictionEngine prediction) {
    this.graph = graph;
    this.prediction = prediction;
  }


  public void buildGraph(final Function<LinkStatePrediction, Long> costSupplier) {
    LOG.trace("buildGraph(costSupplier)");
    final InetAddress localhost = InetAddress.getLoopbackAddress();
    List<LinkStatePrediction> neighborPredictions = cache.get(localhost);
    if (neighborPredictions == null) {
      final Stream<InetAddress> addresses = Node.THIS_INSTANCE.getNeighbors().stream();
      neighborPredictions = this.prediction.getLinkStatePrediction(addresses);
      cache.put(localhost, neighborPredictions);
    }

    for (final LinkStatePrediction neighborPrediction : neighborPredictions) {
      final Vertex<InetAddress> neighbor = this.graph.getUniqueVertex(neighborPrediction.getAddress());
      this.graph.getSource().add(new Edge<>(costSupplier.apply(neighborPrediction), neighbor));
      try {
        List<LinkStatePrediction> neigbors = cache.get(neighborPrediction.getAddress());
        if (neigbors == null) {
          final ExchangeClient client = new ExchangeClient(neighborPrediction.getAddress());
          neigbors = client.getLinkStatePrediction();
          cache.put(neighborPrediction.getAddress(), neigbors);
        }

        for (final LinkStatePrediction neighborOfNeighborPrediction : neigbors) {
          final Vertex<InetAddress> uniqueVertex = this.graph.getUniqueVertex(neighborOfNeighborPrediction.getAddress());
          neighbor.add(new Edge<>(costSupplier.apply(neighborOfNeighborPrediction), uniqueVertex));
        }
      } catch (MalformedURLException | RemoteException | NotBoundException e) {
        LOG.error("Cannot receive prediction of {}: {}", neighbor.data, e);
      }
    }
  }
}
