/**
 * InetConstants.java
 * 11. 3. 2016
 */
package cz.zcu.kiv.fav.overlaynetwork.utils;

import java.net.Inet4Address;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.nio.ByteBuffer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Lukáš Kvídera
 */
public final class InetConstants {
  private static final Logger LOG = LoggerFactory.getLogger(InetConstants.class);

  public static final int SOCKADDR_STORAGE_SIZE = 128;
  public static final short AF_INET = 0x0200; // 2 IN BIG ENDIAN
  public static final short AF_INET6 = 0x0a00; // 10 IN BIG NEDIAN


  public static short getFamily(final InetAddress address) {
    if (address instanceof Inet4Address) {
      return InetConstants.AF_INET;
    }

    if (address instanceof Inet6Address) {
      return InetConstants.AF_INET6;
    }

    throw new SecurityException("invalid address type");
  }

  public static int getAddressLength(final InetAddress address) {
    return getFamily(address) == AF_INET ? 4 : 16;
  }

  public static byte[] getAddressArray(final short family) {
    switch (family) {
      case AF_INET:
        return new byte[4];

      case AF_INET6:
        return new byte[16];

      default:
        LOG.error("Unknown AF!");
        throw new SecurityException("Invalid address family!");
    }
  }

  public static byte[] getAddressArray(final byte[] family) {
    return getAddressArray(convertArrayToShort(family));
  }

  private static short convertArrayToShort(final byte[] array) {
    return ByteBuffer.wrap(array).asShortBuffer().get();
  }

}
