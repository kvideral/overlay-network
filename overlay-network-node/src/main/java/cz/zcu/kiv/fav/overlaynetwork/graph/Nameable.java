/**
 * Nameable.java
 * 6. 3. 2016
 */
package cz.zcu.kiv.fav.overlaynetwork.graph;


/**
 * @author Lukáš Kvídera
 */
public interface Nameable {

  String getName();

}
