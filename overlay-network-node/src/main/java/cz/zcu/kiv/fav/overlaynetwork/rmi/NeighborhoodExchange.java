/**
 * NeighborhoodExchange.java
 * 5. 3. 2016
 */
package cz.zcu.kiv.fav.overlaynetwork.rmi;

import java.net.InetAddress;
import java.rmi.Remote;
import java.rmi.RemoteException;

import cz.zcu.kiv.fav.overlaynetwork.membership.Neighborhood;

/**
 * @author Lukáš Kvídera
 */
public interface NeighborhoodExchange extends Remote {

  /**
   * This method returns list of neighboring instancces.
   * @return
   */
  Neighborhood getNeighbors(InetAddress who) throws RemoteException;
}
