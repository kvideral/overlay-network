/**
 * PredictionCache.java
 * 10. 4. 2016
 */
package cz.zcu.kiv.fav.overlaynetwork.prediction;

import java.net.InetAddress;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Lukáš Kvídera
 */
public class PredictionCache {

  private static final Logger LOG = LoggerFactory.getLogger(PredictionCache.class);
  private static final Map<InetAddress, CacheItem<List<LinkStatePrediction>>> cache = Collections.synchronizedMap(new HashMap<>());
  private static final long TEN_MINUTES = 6 * 10 * 1_000;

  private static class CacheItem<T> {
    final T item;
    final long timestamp;

    public CacheItem(final T item) {
      this.item = item;
      this.timestamp = System.currentTimeMillis();
    }
  }

  public List<LinkStatePrediction> get(final InetAddress address) {
    LOG.trace("get(address={})", address);
    final CacheItem<List<LinkStatePrediction>> cachedPrediction = cache.get(address);

    if (cachedPrediction == null || cachedPrediction.timestamp + TEN_MINUTES < System.currentTimeMillis()) {
      return null;
    }

    return cachedPrediction.item;
  }

  public void put(final InetAddress address, final List<LinkStatePrediction> prediction) {
    LOG.trace("put(address={}, prediction={})", address, prediction);
    cache.put(address, new CacheItem<>(prediction));
  }
}
