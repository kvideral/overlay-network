/**
 * InetSocketReader.java
 * 13. 3. 2016
 */
package cz.zcu.kiv.fav.overlaynetwork.utils;

import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Lukáš Kvídera
 */
public class InetAddressReader {

  private static final Logger LOG = LoggerFactory.getLogger(InetAddressReader.class);
  private final InputStream is;

  public InetAddressReader(final InputStream is) {
    this.is = is;
  }

  public InetAddress read() throws IOException {
    LOG.trace("readAddress(socket)");
    final byte[] family = new byte[2];
    this.is.read(family);

    final byte[] buffer = InetConstants.getAddressArray(family);
    this.is.read(buffer);

    return InetAddress.getByAddress(buffer);
  }
}
