/**
 * ThroughputMeasurementJob.java
 * 23. 3. 2016
 */
package cz.zcu.kiv.fav.overlaynetwork.measurement;

import java.io.IOException;
import java.net.InetAddress;
import java.sql.SQLException;

import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cz.zcu.kiv.fav.overlaynetwork.membership.LinkStateService;
import cz.zcu.kiv.fav.overlaynetwork.membership.Node;
import cz.zcu.kiv.fav.overlaynetwork.membership.StatisticalDataDao;


/**
 * @author Lukáš Kvídera
 */
@DisallowConcurrentExecution
public class ThroughputMeasurementJob implements Job {

  private static final Logger LOG = LoggerFactory.getLogger(ThroughputMeasurementJob.class);

  private final LinkStateService service;


  public ThroughputMeasurementJob() throws SQLException {
    this.service = new LinkStateService(new StatisticalDataDao());
  }

  @Override
  public void execute(final JobExecutionContext context) throws JobExecutionException {
    LOG.trace("execute()");
    final ThroughputService throughputService = new ThroughputService();
    for (final InetAddress destination : Node.THIS_INSTANCE.getNeighbors()) {
      try {
        final long throughput = throughputService.measure(destination);
        LOG.info("throughput -> {} = {} kB/s", destination, throughput);
        this.service.reportThroughput(destination, throughput);
      } catch (final IOException e) {
        LOG.error("Cannot measuer throughput to {}.", destination);
      }
    }
  }

}
