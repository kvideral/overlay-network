/**
 * PredictionService.java
 * 6. 3. 2016
 */
package cz.zcu.kiv.fav.overlaynetwork.prediction;

import static java.lang.Long.MAX_VALUE;
import static java.lang.Long.MIN_VALUE;

import java.net.InetAddress;
import java.sql.SQLException;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cz.zcu.kiv.fav.overlaynetwork.membership.LinkReport;
import cz.zcu.kiv.fav.overlaynetwork.membership.LinkReport.Delay;
import cz.zcu.kiv.fav.overlaynetwork.membership.LinkReport.Throughput;
import cz.zcu.kiv.fav.overlaynetwork.membership.StatisticalDataProvider;

/**
 * @author Lukáš Kvídera
 */
public class PredictionEngine {

  private static final Logger LOG = LoggerFactory.getLogger(PredictionEngine.class);
  private static final PredictionCache cache = new PredictionCache();

  private final StatisticalDataProvider provider;
  private final PredictionCalculator predictor;

  /**
   * @param provider dao for measured data
   */
  public PredictionEngine(final StatisticalDataProvider provider, final PredictionCalculator predictor) {
    this.provider = provider;
    this.predictor = predictor;
  }

  public static PredictionEngine getArimaInstance(final StatisticalDataProvider provider) {
    return new PredictionEngine(provider, new ArimaCalculator());
  }

  public LinkStatePrediction getPredictionFor(final InetAddress address) {
    LOG.trace("getPredictionFor(address={})", address);
    try {
      final LinkReport report = this.provider.getReport(address);
      final List<Delay> delayTimeSerie = report.getDelayTimeSerie();
      final List<Throughput> throughputTimeSerie = report.getThroughputTimeSerie();

      final Prediction delayPrediction = this.predictor.calculate(delayTimeSerie);
      final Prediction packetlossPrediction = this.predictor.calculate(throughputTimeSerie);
      return new LinkStatePrediction(address, delayPrediction.getAsLong(), packetlossPrediction.getAsLong(), 0);
    } catch (final SQLException e) {
      LOG.error("Cannot load data from database!", e);
    }

    return new LinkStatePrediction(address, MAX_VALUE, MAX_VALUE, MIN_VALUE);
  }

  public List<LinkStatePrediction> getLinkStatePrediction(final Stream<InetAddress> addresses) {
    LOG.trace("getLinkStatePrediction(addresses)");
    final List<LinkStatePrediction> cachedPrediction = cache.get(InetAddress.getLoopbackAddress());
    if (cachedPrediction != null) {
      return cachedPrediction;
    }

    final List<LinkStatePrediction> prediction = addresses
        .map(address -> this.getPredictionFor(address))
        .collect(Collectors.toList());
    cache.put(InetAddress.getLoopbackAddress(), prediction);
    return prediction;
  }
}
