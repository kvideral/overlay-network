#!/bin/sh


MASTER_IP="172.17.0.2"
THIS_IP=$(ip a | grep inet | tail -n 2 | head -n 1 | tr -s " " | cut -d " " -f 3 | cut -d "/" -f 1)
JAVA_OPTS="-Djava.library.path=.:/usr/lib/R/site-library/rJava/jri/ -Djava.net.preferIPv6Addresses=true -DmasterAddress=$MASTER_IP -DthisAddress=$THIS_IP"
JAR_NAME="overlay-network-node-0.0.1-SNAPSHOT-jar-with-dependencies.jar"

echo $JAVA_OPTS

./overlay-network-proxy 1080 &
./overlay-network-ping &
java $JAVA_OPTS -jar $JAR_NAME
