#!/bin/bash

echo "Starting overlay network..-"
./start-overlay-nodes.sh

echo "Starting measurement..."
./start-measurement.sh


echo "Starting link retarder..."
DEVS=$(ip a | egrep -o "veth[a-zA-Z0-9]+")

BASE="172.17.0."
IP=2

for dev in $DEVS
do 
  ARGS="$ARGS $dev ${BASE}$((IP++))"
done

echo $ARGS
sudo ./simulation.py $ARGS
