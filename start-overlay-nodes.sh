#!/bin/bash

for i in {1..5}
do
  docker run -d --name overlay-node-$i --tmpfs /tmp:exec -v /mnt/sandbox:/mnt/sandbox overlay-network ./startup.sh
done

