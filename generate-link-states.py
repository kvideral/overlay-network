#!/usr/bin/python3

import random
from collections import deque


def read_delay_serie():
  with open("/tmp/time-serie.dat") as data_source:
    return [int(row.split()[1]) for row in data_source.read().splitlines()]

def read_throughput_serie():
  with open("/tmp/throughput-serie.dat") as data_source:
    return [int(row.split()[1]) for row in data_source.read().splitlines()]


def print_time_serie(filename, ts):
  with open(filename, 'w') as output_file:
    for val in ts:
      output_file.write("%s\n" % str(val))


def shuffle_time_serie(time_serie):
  offset = random.randint(0, 30) * 300
  shufled_ts = deque(time_serie)
  shufled_ts.rotate(offset)
  return shufled_ts


def get_path(ip1, ip2):
  base_ip = "172.17.0."
  return "/tmp/" + base_ip + str(ip1) + "-" + base_ip + str(ip2)

def normalize(time_serie, k):
  max_value = max(time_serie)
  return [max(int(k * val / max_value), 10) for val in time_serie]


def generate_links():
  for ip1 in range(1, 10):
    for ip2 in range(1, 10):
      if ip1 == ip2:
        continue

      path = get_path(ip1, ip2)
      filename = path + ".delay.ts" 
      print_time_serie(filename, normalize(shuffle_time_serie(delay_serie), 400))
      filename = path + ".throughput.ts"
      print_time_serie(filename, normalize(shuffle_time_serie(throughput_serie), 1000))
  
  the_same_delay_ts = normalize(shuffle_time_serie(delay_serie), 1000)
  print_time_serie(get_path(7, 8) + ".delay.ts", the_same_delay_ts)
  print_time_serie(get_path(8, 7) + ".delay.ts", the_same_delay_ts)

  print_time_serie(get_path(3, 7) + ".delay.ts", the_same_delay_ts)
  print_time_serie(get_path(7, 3) + ".delay.ts", the_same_delay_ts)

  the_same_throughput_ts = normalize(shuffle_time_serie(throughput_serie), 1000)
  print_time_serie(get_path(7, 8) + ".throughput.ts", the_same_throughput_ts)
  print_time_serie(get_path(8, 7) + ".throughput.ts", the_same_throughput_ts)

  print_time_serie(get_path(7, 3) + ".throughput.ts", the_same_throughput_ts)
  print_time_serie(get_path(3, 7) + ".throughput.ts", the_same_throughput_ts)




random.seed(0)
delay_serie = read_delay_serie()
throughput_serie = read_throughput_serie()
generate_links()

