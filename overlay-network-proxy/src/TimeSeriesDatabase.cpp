///*----------------------------------------------------------------------------
// * SQLiteDatabase.cpp
// *
// *  Created on: 28. 2. 2016
// *      Author: Lukáš Kvídera
// *      Jabber:  segfault@dione.zcu.cz
// ---------------------------------------------------------------------------*/
//
//#include <sqlite3.h>
//#include <string>
//#include <stdexcept>
//#include <netinet/in.h>
//#include <arpa/inet.h>
//
//
//#include "TimeSeriesDatabase.h"
//
//
//TimeSeriesDatabase::TimeSeriesDatabase()
//{
//  if (sqlite3_open_v2(db_path, &db, SQLITE_OPEN_READWRITE, nullptr) != SQLITE_OK)
//  {
//    if (sqlite3_open_v2(db_path, &db, SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE, nullptr) != SQLITE_OK)
//    {
//      throw std::runtime_error {"Cannot open database file! " + std::string { sqlite3_errmsg(db) } };
//    }
//
//    if (sqlite3_exec(db, "PRAGMA journal_mode=WAL", nullptr, nullptr, nullptr) != SQLITE_OK)
//    {
//      throw std::runtime_error {"Cannot change write mode! " + std::string { sqlite3_errmsg(db) } };
//    }
//
//    if (sqlite3_exec(db,
//        "CREATE TABLE 'main'.'stats' ( "
//        "id INTEGER PRIMARY KEY ,      "
//        "ipaddress      TEXT NOT NULL, "
//        "packetloss         INT,       "
//        "delay              INT,       "
//        "date               TIMESTAMP  "
//        "  DEFAULT CURRENT_TIMESTAMP   "
//        ")                             ",
//        nullptr,
//        nullptr,
//        nullptr) != SQLITE_OK)
//    {
//      throw std::runtime_error {"Cannot create table! " + std::string { sqlite3_errmsg(db) } };
//    }
//  }
//
//}
//
//TimeSeriesDatabase::~TimeSeriesDatabase()
//{
//  sqlite3_close_v2(db);
//}
//
//void TimeSeriesDatabase::insert_delay(const sockaddr_storage& address, const clock_t delay)
//{
//  sqlite3_stmt* stmt;
//  if (sqlite3_prepare_v2(db, "INSERT INTO stats(ipaddress, delay) VALUES (?, ?)", -1, &stmt, nullptr) != SQLITE_OK)
//  {
//    throw std::runtime_error("Cannot prepare statement! " + std::string { sqlite3_errmsg(db) });
//  }
//
//  char ip_address_text[256];
//  inet_ntop(address.ss_family, address.ss_family == AF_INET ?
//      (const void *) &(reinterpret_cast<const sockaddr_in &>(address).sin_addr) :
//      (const void *) &(reinterpret_cast<const sockaddr_in6 &>(address).sin6_addr)
//      , ip_address_text, sizeof ip_address_text);
//  sqlite3_bind_text(stmt, 1, ip_address_text, -1, SQLITE_STATIC);
//  sqlite3_bind_int(stmt, 2, delay);
//  int code;
//  if ((code = sqlite3_step(stmt)) != SQLITE_DONE)
//  {
//    throw std::runtime_error("Cannot execute statement! " + std::to_string(code) + std::string { sqlite3_errmsg(db) });
//  }
//  sqlite3_finalize(stmt);
//}
//
//void TimeSeriesDatabase::insert_packetloss(const sockaddr_storage& address, const uint32_t packets_lost)
//{
//  sqlite3_stmt* stmt;
//  if (sqlite3_prepare_v2(db, "INSERT INTO stats(ipaddress, packetloss) VALUES (?, ?)", -1, &stmt, nullptr) != SQLITE_OK)
//  {
//    throw std::runtime_error("Cannot prepare statement! " + std::string { sqlite3_errmsg(db) });
//  }
//
//  char ip_address_text[256];
//  inet_ntop(address.ss_family, address.ss_family == AF_INET ?
//      (const void *) &(reinterpret_cast<const sockaddr_in &>(address).sin_addr) :
//      (const void *) &(reinterpret_cast<const sockaddr_in6 &>(address).sin6_addr),
//      ip_address_text, sizeof ip_address_text);
//  sqlite3_bind_text(stmt, 1, ip_address_text, -1, SQLITE_STATIC);
//  sqlite3_bind_int(stmt, 2, packets_lost);
//  int code;
//  if ((code = sqlite3_step(stmt)) != SQLITE_DONE)
//  {
//    throw std::runtime_error("Cannot execute statement! " + std::to_string(code) + std::string { sqlite3_errmsg(db) });
//  }
//  sqlite3_finalize(stmt);
//}
