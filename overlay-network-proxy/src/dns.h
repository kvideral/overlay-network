/*----------------------------------------------------------------------------
 * dns.h
 *
 *  Created on: 9. 3. 2016
 *      Author: Lukáš Kvídera
 *      Jabber:  segfault@dione.zcu.cz
 ---------------------------------------------------------------------------*/
#ifndef SRC_DNS_H_
#define SRC_DNS_H_

#include <sys/socket.h>

int dns_lookup(const char * hostname, sockaddr_storage * address_to_fill);


#endif /* SRC_DNS_H_ */
