/*----------------------------------------------------------------------------
 * ProxyPipeline.cpp
 *
 *  Created on: 27. 2. 2015
 *      Author: Lukáš Kvídera
 *      Jabber: segfault@dione.zcu.cz
 ---------------------------------------------------------------------------*/

#include <chrono>
#include <thread>

#include <tbb/tbb.h>

#include "ProxyPipeline.h"
#include "Connection.h"
#include "Logger.h"
#include "sock5/socks5.h"
#include "utils.h"

ProxyPipeline::ProxyPipeline(const std::string port_str)
: listener { (port) std::atoi(port_str.c_str()) },
  router { port_str }
{
  run();
}

void fill_proxy_port(sockaddr_storage& sock_proxy_address)
{
  Logger::trace("Filling proxy port");
  switch (sock_proxy_address.ss_family) {
    case AF_INET:
      reinterpret_cast<sockaddr_in&>(sock_proxy_address).sin_port = htons(1080);
      break;

    case AF_INET6:
      reinterpret_cast<sockaddr_in6&>(sock_proxy_address).sin6_port = htons(1080);
      break;

    default:
      throw std::runtime_error("Unsupported family!");
  }
}

uint16_t get_port(const sockaddr_storage& sock_proxy_address)
{
  Logger::trace("Filling proxy port");
  switch (sock_proxy_address.ss_family) {
    case AF_INET:
      return reinterpret_cast<const sockaddr_in&>(sock_proxy_address).sin_port;
      break;

    case AF_INET6:
      return reinterpret_cast<const sockaddr_in6&>(sock_proxy_address).sin6_port;
      break;

    default:
      throw std::runtime_error("Unsupported family!");
  }
}

void ProxyPipeline::run(void)
{
  tbb::parallel_pipeline(
    64,
    tbb::make_filter<void,shared_connection>(
      tbb::filter::parallel,
      [this](tbb::flow_control& fc)
      {
        return shared_connection { new Connection { listener.accept() } };
      }
    )
    &
    tbb::make_filter<shared_connection,shared_endpoints>(
      tbb::filter::parallel,
      [this](const shared_connection client) {
        try {
          socks::accept(client);
          socks::socks server_address { socks::get_address(client) };

          sockaddr_storage next_hop_address = router.get_next_hop(
              server_address.address,
              get_port(server_address.address)
              );
          const bool is_same_address = cmpaddr(server_address.address, next_hop_address);

          if (!is_same_address) {
            fill_proxy_port(next_hop_address);
          } else {
            /** overwrites port, ip address is the same */
            next_hop_address = server_address.address;
          }

          shared_connection server { new Connection { next_hop_address } };
          return shared_endpoints {
            new end_points {
              server_address,
              client,
              server,
              is_same_address
            }
          };
        } catch (const std::runtime_error& ex) {
          Logger::error(ex.what());
          return shared_endpoints {
            new end_points {
              socks::socks { },
              client,
            }
          };
        }
      }
    )
    &
    /**
     * Send response with status code to the client.
     */
    tbb::make_filter<shared_endpoints,shared_endpoints>(
      tbb::filter::parallel,
      [this](const shared_endpoints endpoint) {
        static const constexpr size_t SOCKS_HEADER_SIZE = sizeof(socks::socks::header) + 4 + 2;
        char buf[sizeof(socks::socks)];

        if (endpoint->server == nullptr) {
          reinterpret_cast<socks::socks&>(*buf) = { 5, 3, 0, 1 };
        } else {
          reinterpret_cast<socks::socks&>(*buf) = { 5, 0, 0, 1 };

          Logger::debug("Direct connection = " + std::to_string(endpoint->direct));
          if (!endpoint->direct) {
            if (!socks::connect(endpoint->server, endpoint->header)) {
              endpoint->server = nullptr;
            }
          }
        }

        /* This method does not necessary send port from it's offset,
         * but all fields are zeroed so it does not matter */
        endpoint->client->send(buf, SOCKS_HEADER_SIZE);
        return endpoint;
      }
    )
    &
    tbb::make_filter<shared_endpoints,void>(
      tbb::filter::parallel,
      [&](const shared_endpoints endpoint) {
        if (endpoint->server == nullptr) {
          return;
        }

        tunnel.forward(endpoint->client , endpoint->server);
      }
    )
  );
}
