/*----------------------------------------------------------------------------
 * ProxyPipeline.h
 *
 *  Created on: 27. 2. 2015
 *      Author: Lukáš Kvídera
 *      Jabber: segfault@dione.zcu.cz
 ---------------------------------------------------------------------------*/
#ifndef PROXYPIPELINE_H_
#define PROXYPIPELINE_H_

#include <memory>

#include "ListeningSocket.h"
#include "Connection.h"
#include "sock5/socks5.h"
#include "ProxyTunnel.h"
#include "Router.h"


/*
 *
 */
class ProxyPipeline
{
  public:
    ProxyPipeline(const std::string port);

  private:
    void run(void);

    const ListeningSocket& listener;
    ProxyTunnel tunnel;
    Router router;
};

struct end_points {
    socks::socks header;
    shared_connection client;
    shared_connection server;
    bool direct;
};

using shared_endpoints = std::shared_ptr<end_points>;

#endif /* PROXYPIPELINE_H_ */
