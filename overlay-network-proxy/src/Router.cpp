/*----------------------------------------------------------------------------
 * Router.cpp
 *
 *  Created on: 10. 10. 2015
 *      Author: Lukáš Kvídera
 *     Version: 0.0
 *    Jabber:  segfault@dione.zcu.cz
 ---------------------------------------------------------------------------*/

#include <iostream>
#include <cstring>
#include <set>
#include <algorithm>

#include <unistd.h>
#include <sys/un.h>

#include "Router.h"
#include "Logger.h"
#include "utils.h"
#include "config.h"
#include "qos.h"

const std::string print_neighbor(const neighbor& neighbor)
{
  return to_string(neighbor.address.uni);
}

Router::Router(const std::string& port_str) :
socket { (port) std::atoi(port_str.c_str()), SOCK_DGRAM, IPPROTO_UDP },
internal_route_update_thread { &Router::receive_routing_table, this }
{ }

/**
 * Returns next hop or given destination, if the route is faster.
 *
 * If rout is missing or old, then delay is measured directly from this router
 * and indirectly from other neighboring routers.
 *
 * The fastest route is installed and chosen as route for current connection.
 *
 * @param destination
 * @return next hop or direct address
 */
sockaddr_storage Router::get_next_hop(const sockaddr_storage& destination, const uint16_t port)
{
  routing_table_ptr routes_;
  switch (qos::classify(port)) {
    case qos::DELAY:
      Logger::trace("QoS DELAY");
      routes_ = delay_routes;
      break;

    case qos::THROUGHPUT:
      Logger::trace("QoS THROUGHPUT");
      routes_ = delay_routes;
      break;

    default:
      throw std::runtime_error("Invalid QoS class!");
  }

  const auto address_comparator = [&](const route& r) {
     return reinterpret_cast<const sockaddr_in&>(r.address).sin_addr.s_addr
            ==
            reinterpret_cast<const sockaddr_in&>(destination).sin_addr.s_addr;
   };


   const auto route = std::find_if(routes_->begin(), routes_->end(), address_comparator);

   /* route not found */
   if (route == routes_->end()) {
     request_route_for(destination);
     const auto routes_ = delay_routes;
     return std::find_if(delay_routes->begin(), delay_routes->end(), address_comparator)->next_hop;
 //    return destination;
   }

   Logger::trace("Route found -> " + to_string(route->next_hop));
   for (const auto& r : *delay_routes) {
     Logger::trace("Route " + to_string(r.address) + " -> " + to_string(r.next_hop));
   }
   return route->next_hop;
}

void Router::handle_pingrequest(
    const sockaddr_storage& address_to_measure,
    const sockaddr_storage& remote_address,
    const socklen_t addr_len)
{
  clock_t delay = measure_external_delay(address_to_measure);
  router_header header { VERSION, PING_REPLY };
  header.timestamp = delay;
  socket.sendto(&header, sizeof header, &remote_address, addr_len);
}

void receive_ipv4(const Connection& connection, sockaddr_storage& ipv4_storage)
{
  auto& ipv4 = reinterpret_cast<sockaddr_in&>(ipv4_storage);
  connection.recv(&ipv4.sin_addr, sizeof ipv4.sin_addr, true);
  Logger::trace("ipv4 " + to_string(ipv4_storage));
}

void receive_ipv6(const Connection& connection, sockaddr_storage& ipv6_storage)
{
  auto& ipv6 = reinterpret_cast<sockaddr_in6&>(ipv6_storage);
  connection.recv(&ipv6.sin6_addr, sizeof ipv6.sin6_addr, true);
  Logger::trace("ipv6" + to_string(ipv6_storage));
}

void receive_address(const Connection& connection, sockaddr_storage& address_storage)
{
  if (address_storage.ss_family == AF_INET) {
    receive_ipv4(connection, address_storage);
  } else if (address_storage.ss_family == AF_INET6) {
    receive_ipv6(connection, address_storage);
  }
}

void send_ipv4(const Connection& connection, const sockaddr_storage& ipv4_storage)
{
  auto& ipv4 = reinterpret_cast<const sockaddr_in&>(ipv4_storage);
  connection.send(&ipv4.sin_addr, sizeof ipv4.sin_addr);
  connection.send(&ipv4.sin_port, sizeof ipv4.sin_port);
  Logger::trace("ipv4 " + to_string(ipv4_storage));
}

void send_ipv6(const Connection& connection, const sockaddr_storage& ipv6_storage)
{
  auto& ipv6 = reinterpret_cast<const sockaddr_in6&>(ipv6_storage);
  connection.send(&ipv6.sin6_addr, sizeof ipv6.sin6_addr);
  connection.send(&ipv6.sin6_port, sizeof ipv6.sin6_port);
  Logger::trace("ipv6" + to_string(ipv6_storage));
}

void send_address(const Connection& connection, const sockaddr_storage& address_storage)
{
  connection.send(&address_storage.ss_family, sizeof address_storage.ss_family);
  if (address_storage.ss_family == AF_INET) {
    send_ipv4(connection, address_storage);
  } else if (address_storage.ss_family == AF_INET6) {
    send_ipv6(connection, address_storage);
  }
}

void Router::receive_routing_table(void)
{
  ListeningSocket remote_routing_table { 1082, SOCK_STREAM, IPPROTO_TCP };
  while (true)
  {
    Connection connection = remote_routing_table.accept();
    sockaddr_storage destination;
    auto table = std::make_shared<routing_table>();
    routing_table_ptr* swap_table;

    uint8_t table_type = -1;
    if (!connection.recv(&table_type, sizeof table_type, true)) {
      continue;
    } else {
      switch (table_type) {
        case qos::DELAY:
          swap_table = &delay_routes;
          break;

        case qos::THROUGHPUT:
          swap_table = &throughput_routes;
          break;

        default:
          Logger::error("Unknown QoS table " + std::to_string(table_type));
          break;
      }
    }

    while (connection.recv(&destination.ss_family, sizeof destination.ss_family, true) > 0)
    {
      receive_address(connection, destination);

      sockaddr_storage next_hop;
      if (connection.recv(&next_hop.ss_family, sizeof next_hop.ss_family, true) <= 0) {
        Logger::error("Cannot receive next hop!");
        break;
      }

      receive_address(connection, next_hop);
      table->insert({destination, next_hop});
    }

    *swap_table = table;
    for (const auto& r : *delay_routes) {
      Logger::trace("Route " + to_string(r.address) + " -> " + to_string(r.next_hop));
    }
  }
}


clock_t Router::measure_external_delay(const sockaddr_storage& destination)
{
   Logger::trace("Measuring external ping -> " + to_string(destination));

   sockaddr_un unix_sock_addr { AF_UNIX };
   std::strcpy(unix_sock_addr.sun_path, "/var/run/overlay-network.sock");

   int sock = ::socket(AF_UNIX, SOCK_STREAM, 0);

   if (sock < 0) {
     Logger::error("Cannot obtain UNIX socket!");
     return -1;
   }

   if (connect(sock, reinterpret_cast<const sockaddr*>(&unix_sock_addr), sizeof unix_sock_addr) < 0) {
     std::perror("UNIX socket");
     Logger::error("Cannot connect to UNIX socket!");
     return -1;
   }

   send(sock, &destination, sizeof destination, MSG_WAITALL);

   clock_t delay;
   recv(sock, &delay, sizeof delay, MSG_WAITALL);

   Logger::debug("Delay = " + std::to_string(delay));
   close(sock);
   return delay;
}

void Router::request_route_for(const sockaddr_storage& destination) {
  try {
    sockaddr_storage localhost { };
    auto& localhost_ = reinterpret_cast<sockaddr_in6&>(localhost);
    localhost_.sin6_family = AF_INET6;
    localhost_.sin6_port = htons(1083);
    localhost_.sin6_addr = in6addr_loopback;

    Connection connection { localhost };
    connection.setTimeout(100); //FIXME
    sockaddr_storage next_hop;
    send_address(connection, destination);

    if (connection.recv(&next_hop.ss_family, sizeof next_hop.ss_family, true) <= 0) {
      Logger::error("Cannot receive next hop!");
      delay_routes->insert({destination, destination}); //FIXME connects with sock5 header
      return;
    }

    receive_address(connection, next_hop);
    delay_routes->insert({destination, next_hop});
  } catch (const std::runtime_error& ex) {
    throw std::runtime_error("Cannot communicate with routing table provider!");
  }
}
