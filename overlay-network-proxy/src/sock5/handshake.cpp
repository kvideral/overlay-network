/*----------------------------------------------------------------------------
 * handshake.cpp
 *
 *  Created on: 11. 10. 2015
 *      Author: Lukáš Kvídera
 *      Jabber:  segfault@dione.zcu.cz
 ---------------------------------------------------------------------------*/

#include <cstring>

#include "socks5.h"
#include "../dns.h"
#include "../Connection.h"
#include "../Logger.h"

namespace socks {
  void accept(const shared_connection& connection)
  {
    auth_query auth_query;
    static constexpr const size_t header_size = sizeof(auth_query.ver) + sizeof(auth_query.n_methods);
    if (connection->recv(&auth_query, header_size) == header_size)
    {
      if (auth_query.n_methods >= 255) {
        throw std::runtime_error("Malformed SOCKS handshake!");
      }
      connection->recv(reinterpret_cast<char*>(&auth_query) + header_size, auth_query.n_methods * sizeof(auth_method));
      auth_reply auth_reply { 5, 0 };
      connection->send(&auth_reply, sizeof(auth_reply));
    }
    else
    {
      //TODO exception
    }
  }

  bool connect(const shared_connection& connection, const socks& socks_header)
  {
    auth_query auth_query { 5, 1, 0 };
    static const constexpr size_t AUTH_QUERY_SIZE =
        sizeof(auth_query.ver) + sizeof(auth_query.n_methods) + sizeof(auth_method);
    connection->send(&auth_query, AUTH_QUERY_SIZE);

    auth_reply auth_reply { 0 , 255 };
    connection->recv(&auth_reply, sizeof(auth_reply));

    if (auth_reply.method != 0) {
      Logger::debug("Received unsupported auth method in reply from server! " + std::to_string(auth_reply.method));
      return false;
    }

    static const constexpr size_t SOCKS_HEADER_SIZE = sizeof(socks::header) + 4 + 2;
    char header[SOCKS_HEADER_SIZE];
    auto position = header;
    const auto& address = reinterpret_cast<const sockaddr_in&>(socks_header.address);
    std::memcpy(header, &socks_header.header, sizeof(socks_header.header));
    position += sizeof(socks_header.header);
    std::memcpy(position, &address.sin_addr, 4);
    position += 4;
    std::memcpy(position, &address.sin_port, 2);
    connection->send(header, SOCKS_HEADER_SIZE);

    socks server_reply;
    connection->recv(&server_reply, SOCKS_HEADER_SIZE);

    Logger::trace("Received SOCKS reply from server = " + std::to_string(server_reply.header.rep));
    return server_reply.header.rep == 0;
  }

  socks get_address(const shared_connection& connection)
  {
    socks data;
    static constexpr const size_t header_size = sizeof(data.header);
    if (connection->recv(&data, header_size) == header_size)
    {
      switch (data.header.atyp)
      {
        case IPv4:
          connection->recv(&reinterpret_cast<sockaddr_in*>(&data.address)->sin_addr, 4);
          connection->recv(&reinterpret_cast<sockaddr_in*>(&data.address)->sin_port, 2);
          data.address.ss_family = AF_INET;
          Logger::debug("Received IPv4");
          break;

        case IPv6:
          connection->recv(&reinterpret_cast<sockaddr_in6*>(&data.address)->sin6_addr, 16);
          connection->recv(&reinterpret_cast<sockaddr_in6*>(&data.address)->sin6_port, 2);
          data.address.ss_family = AF_INET6;
          Logger::debug("Received IPv6");
          break;

        case DOMAIN_NAME:
        {
          std::array<char, 256> domain_name { };
          int port = 0;
          connection->recv(domain_name.begin(), 1); // fill length of domain name
          if (domain_name.front() >= 255) {
            throw std::runtime_error("Malformed packet! Potential buffer overflow!");
          }
          connection->recv(domain_name.begin(), domain_name.front()); // read the length
          Logger::error(std::string {"Received domain name: "} + domain_name.begin());
          connection->recv(&port, 2);
          dns_lookup(domain_name.begin(), &data.address);
          switch (data.address.ss_family) {
            case AF_INET:
              reinterpret_cast<sockaddr_in*>(&data.address)->sin_port = port;
              data.header.atyp = IPv4;
              break;
            case AF_INET6:
              reinterpret_cast<sockaddr_in6*>(&data.address)->sin6_port = port;
              data.header.atyp = IPv6;
              break;
            default:
              throw std::runtime_error("Unsupported address family");
              break;
          }
        }
          break;

        default:
          throw std::runtime_error("Unsupported address type");
          break;
      }
      Logger::debug("Received port " + std::to_string(ntohs(reinterpret_cast<sockaddr_in*>(&data.address)->sin_port)));
    }
    else
    {
      throw std::runtime_error("Malformed packet!");
    }
    return data;
  }
}
