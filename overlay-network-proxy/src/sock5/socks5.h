/*----------------------------------------------------------------------------
 * socks5.h
 *
 *  Created on: 10. 10. 2015
 *      Author: Lukáš Kvídera
 *     Version: 0.0
 *    Jabber:  segfault@dione.zcu.cz
 ---------------------------------------------------------------------------*/
#ifndef SRC_SOCK5_SOCKS5_H_
#define SRC_SOCK5_SOCKS5_H_

#include <cstdint>
#include <arpa/inet.h>

#include "../Connection.h"

using port = uint16_t;

/**
 * https://www.ietf.org/rfc/rfc1928.txt
 */
namespace socks
{
 /**
  * protocol version: X'05'
  */
  using version = uint8_t;

 /**
  *  o  CONNECT X'01'
  *  o  BIND X'02'
  *  o  UDP ASSOCIATE X'03'
  */
  using command = uint8_t;

 /**
  *  o  X'00' succeeded
  *  o  X'01' general SOCKS server failure
  *  o  X'02' connection not allowed by ruleset
  *  o  X'03' Network unreachable
  *  o  X'04' Host unreachable
  *  o  X'05' Connection refused
  *  o  X'06' TTL expired
  *  o  X'07' Command not supported
  *  o  X'08' Address type not supported
  *  o  X'09' to X'FF' unassigned
  */
  using reply = uint8_t;

  enum atype {
    IPv4 = 1,
    DOMAIN_NAME = 3,
    IPv6 = 4,
  };

 /**
  *  o  IP V4 address: X'01'
  *  o  DOMAINNAME: X'03'
  *  o  IP V6 address: X'04'
  */
  using address_type = uint8_t;

 /**
  *  o  X'00' NO AUTHENTICATION REQUIRED
  *  o  X'01' GSSAPI
  *  o  X'02' USERNAME/PASSWORD
  *  o  X'03' to X'7F' IANA ASSIGNED
  *  o  X'80' to X'FE' RESERVED FOR PRIVATE METHODS
  *  o  X'FF' NO ACCEPTABLE METHODS
  */
  using auth_method = uint8_t;


  struct socks
  {
    struct {
      version ver;

      union
      {
        command cmd;
        reply rep;
      };

      /**
       * RESERVED
       */
      uint8_t rsv;
      address_type atyp;
    } header;

    /**
     * desired destination address
     */
    sockaddr_storage address;
  };

  struct auth_query
  {
    version ver;
    uint8_t n_methods;
    auth_method methods[255];
  };

  struct auth_reply
  {
    version ver;
    auth_method method;
  };

  void accept(const shared_connection& connection);
  socks get_address(const shared_connection& connection);
  bool connect(const shared_connection& connection, const socks& socks_header);
}
#endif /* SRC_SOCK5_SOCKS5_H_ */
