/*----------------------------------------------------------------------------
 * dns.c
 *
 *  Created on: 9. 3. 2016
 *      Author: Lukáš Kvídera
 *      Jabber:  segfault@dione.zcu.cz
 ---------------------------------------------------------------------------*/
#include <cstring>
#include <cerrno>
#include <cinttypes>
#include <stdexcept>

#include <netdb.h>
#include <sys/socket.h>

int dns_lookup(const char * hostname, sockaddr_storage * address_to_fill)
{
  addrinfo hints = { };
  addrinfo *result;

  memset(&hints, 0, sizeof hints); // make sure the struct is empty
  hints.ai_family = AF_INET; //AF_UNSPEC;    /* Allow IPv4 or IPv6 */
  hints.ai_socktype = SOCK_STREAM; /* TCP socket */


  if (getaddrinfo(hostname, nullptr, &hints, &result) != 0 )
  {
    throw std::runtime_error(std::string { "DNS: " } + gai_strerror(errno));
  }

  memcpy(address_to_fill, result->ai_addr, result->ai_addrlen);
  freeaddrinfo(result);

  return 0;
}

