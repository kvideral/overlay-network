/*----------------------------------------------------------------------------
 * Router.h
 *
 *  Created on: 10. 10. 2015
 *      Author: Lukáš Kvídera
 *      Jabber:  segfault@dione.zcu.cz
 ---------------------------------------------------------------------------*/
#ifndef SRC_ROUTER_H_
#define SRC_ROUTER_H_

#include <cstdint>
#include <cmath>
#include <set>
#include <thread>

#include "ListeningSocket.h"
#include "ICMPSocket.h"
#include "utils.h"

struct route {
  sockaddr_storage address;
  mutable sockaddr_storage next_hop;

//  route(const sockaddr_storage& destination, const sockaddr_storage& next_hop) :
//        address { destination },
//        next_hop { next_hop }
//    { }

  inline friend bool operator <(const route& lhs, const route& rhs)
  {
    return
        reinterpret_cast<const sockaddr_in&>(lhs.address).sin_addr.s_addr
        <
        reinterpret_cast<const sockaddr_in&>(rhs.address).sin_addr.s_addr;
  }
};


struct neighbor {

  neighbor()
  { }

  neighbor(const sockaddr_storage& address, clock_t timestamp) : timestamp { timestamp }
  {
    this->address.uni = address;
  }

  neighbor(const neighbor& n) {
    this->address.uni = n.address.uni;
    this->timestamp = n.timestamp;
    this->delay = n.delay;
  }

  union {
      sockaddr_in ipv4;
      sockaddr_in6 ipv6;
      sockaddr_storage uni;
  } address { };
  clock_t timestamp = 0;
  mutable clock_t delay = 0;

  inline friend bool operator <(const  neighbor& lhs, const  neighbor& rhs)
   {
     return
         reinterpret_cast<const sockaddr_in&>(lhs.address).sin_addr.s_addr
         <
         reinterpret_cast<const sockaddr_in&>(rhs.address).sin_addr.s_addr;
   }

  inline friend bool operator ==(const neighbor& lhs, const sockaddr_storage& rhs)
  {
    return
        reinterpret_cast<const sockaddr_in&>(lhs.address).sin_addr.s_addr
        ==
        reinterpret_cast<const sockaddr_in&>(rhs).sin_addr.s_addr;
  }

  inline friend bool operator ==(const neighbor& lhs, const neighbor& rhs)
  {
    return
        reinterpret_cast<const sockaddr_in&>(lhs.address).sin_addr.s_addr
        ==
        reinterpret_cast<const sockaddr_in&>(rhs.address).sin_addr.s_addr;
  }

  friend std::ostream& operator <<(std::ostream& os, const neighbor& n)
  {
    return os << to_string(n.address.uni);
  }
};


enum command {
  PING_ECHO = 1,
  PING_REPLY = 2,
  PING_EXTERNAL = 4,
};


struct router_header {
  uint8_t version = 1;
  command cmd;
  uint8_t addr_version = 4;
  uint8_t addr_len = 4;
  union {
      uint16_t msg_len;
      clock_t timestamp;
  };
};


/*
 *
 */
class Router
{
  using routing_table = std::set<route>;
  using routing_table_ptr = std::shared_ptr<routing_table>;

  public:
    Router(const std::string& port);
    sockaddr_storage get_next_hop(const sockaddr_storage& destination, const uint16_t port);

  private:
    void request_route_for(const sockaddr_storage& destination);
    clock_t measure_external_delay(const sockaddr_storage& destination);
    void receive_routing_table(void);
    void handle_pingrequest(const sockaddr_storage& address,
        const sockaddr_storage& remote_address, const socklen_t addr_len);

    const ListeningSocket socket;
    routing_table_ptr delay_routes = std::make_shared<routing_table>();
    routing_table_ptr throughput_routes = std::make_shared<routing_table>();
    std::thread internal_route_update_thread;
    sockaddr_storage master_address;
    sockaddr_storage this_address;
};

#endif /* SRC_ROUTER_H_ */
