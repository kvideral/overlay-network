/*
 * Logger.cpp
 *
 *  Created on: 22. 2. 2015
 *      Author: Lukáš Kvídera
 */

#include <iostream>
#include <iomanip>
#include <ctime>
#include <array>
#include <mutex>

#include "Logger.h"

Logger::LogLevel Logger::LOG_LEVEL = Logger::TRACE;

void Logger::log(const std::string& msg, LogLevel loglevel)
{
  if (loglevel > LOG_LEVEL) {
    return;
  }

  switch (loglevel)
  {
    case NONE:
      return;

    case ERROR:
      std::clog << "\033[31;1m";
      break;

    case NORMAL:
      std::clog << "\033[34m";
      break;

    case VERBOSE:
      std::clog << "\033[35m";
      break;

    case DEBUG:
      std::clog << "\033[36m";
      break;

    case TRACE:
      std::clog << "\033[37m";
      break;
  }

  auto t = std::time(nullptr);
  const auto tm = std::localtime(&t);

  static std::mutex m;
  std::lock_guard<std::mutex> lg(m);
  std::clog <<  std::put_time(tm, "%b %d %H:%M:%S") << " " << msg << "\033[0m"<< std::endl;
}

void Logger::error(const std::string& msg)
{
  Logger::log(msg, ERROR);
}

void Logger::info(const std::string& msg)
{
  Logger::log(msg, NORMAL);
}

void Logger::debug(const std::string& msg)
{
  Logger::log(msg, DEBUG);
}

void Logger::trace(const std::string& msg)
{
  Logger::log(msg, TRACE);
}

