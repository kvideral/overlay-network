/*
 * ProxyTunnel.h
 *
 *  Created on: 3. 2. 2015
 *      Author: Lukáš Kvídera
 */

#ifndef PROXYTUNNEL_H_
#define PROXYTUNNEL_H_

#include <tbb/tbb.h>

#include "Connection.h"

using tunnel = std::pair<shared_connection, shared_connection>;

class ProxyTunnel
{
  public:
      ProxyTunnel();
      void forward(const shared_connection& client, const shared_connection& server);

  private:
    /**
     * Starts thread pool that pops connections from queue.
     */
    void wait_for_tunnels();

    tbb::concurrent_bounded_queue<tunnel> tunnels;
    std::atomic<uint32_t> free_threads { 2 };
};

#endif /* PROXYTUNNEL_H_ */
