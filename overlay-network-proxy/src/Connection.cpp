/*
 * Connection.cpp
 *
 *  Created on: 3. 2. 2015
 *      Author: Lukáš Kvídera
 */

#include "Connection.h"

#include <cstring>
#include <cstdio>
#include <cinttypes>
#include <stdexcept>
#include <iostream>
#include <ctime>

#include <netdb.h>
#include <unistd.h>
#include <assert.h>

#include "Logger.h"
#include "utils.h"

std::atomic<int> Connection::id_counter_;

const char *cert_key = "/tmp/private.key";
const char *cert = "/tmp/server.crt";

Connection::Connection(void)
: id_ { id_counter_++ }
{}

Connection::Connection(const sockaddr_storage& address, bool use_ssl)
: id_ { id_counter_++ },
  address { address },
  use_ssl { use_ssl }
{
  sockfd_ = socket(address.ss_family, SOCK_STREAM, 0);
  if (sockfd_ == -1) {
    throw std::runtime_error(std::string("[Connection] ") += strerror(errno) + to_string(address));
  }

  if (::connect(sockfd_, reinterpret_cast<const sockaddr*>(&address), address.ss_family ==
      AF_INET ? sizeof(sockaddr_in) : sizeof(sockaddr_in6)) == -1) {
    throw std::runtime_error(std::string("[Connection] ") += strerror(errno) + to_string(address));
  }

  setDefaultTimeout();
  Logger::log("[Connection] connected id = " + std::to_string(id_), Logger::VERBOSE);
}

Connection::Connection(int sockfd, const sockaddr_storage& address, bool use_ssl)
: sockfd_ { sockfd },
  id_ { id_counter_++ },
  address { address },
  use_ssl { use_ssl }
{
  Logger::log("[Connection] accepted id = " + std::to_string(id_), Logger::VERBOSE);
}

Connection::Connection(Connection&& connection)
: id_{id_counter_++}
{
  std::swap(this->sockfd_, connection.sockfd_);
}

Connection::Connection(const std::string& hostname, const std::string& port, bool use_ssl)
: id_ { id_counter_++ },
  use_ssl { use_ssl }
{
  addrinfo hints { };
  addrinfo *result, *rp;
  bool connected = false;
  int res;

  hints.ai_family = AF_UNSPEC;    /* Allow IPv4 or IPv6 */
  hints.ai_socktype = SOCK_STREAM; /* TCP socket */


  if ( (res = getaddrinfo(hostname.c_str(), port.c_str(), &hints, &result)) != 0 )
  {
    throw std::runtime_error(std::string("[Connection] ") += gai_strerror(res));
  }

  for (rp = result; rp != nullptr; rp = rp->ai_next)
  {
    sockfd_ = socket(rp->ai_family, rp->ai_socktype, rp->ai_protocol);

    if (sockfd_ == -1) {
      continue;
    }

    if (::connect(sockfd_, rp->ai_addr, rp->ai_addrlen) != -1) {
      connected = true;
      break;
    }

    close(sockfd_);
  }

  freeaddrinfo(result);

  if (!connected) {
    throw std::runtime_error(std::string("[Connection] ") += strerror(errno));
  } else {
    Logger::log("[Connection] id = " + std::to_string(id_), Logger::VERBOSE);
  }

  setDefaultTimeout();

  if (use_ssl) {
      connectSSL();
  }
}

Connection::~Connection(void)
{
  Logger::log("[Connection] destructed id = " + std::to_string(id_), Logger::DEBUG);

  if (sockfd_ > 0) {
    shutdown(SHUT_RDWR);
    close(sockfd_);
  }

  if (ssl) {
    SSL_free(ssl);
  }

  if (ctx) {
    SSL_CTX_free(ctx);
  }
}

Connection& Connection::operator =(Connection&& connection)
{
  std::swap(this->sockfd_, connection.sockfd_);
  return *this;
}

bool Connection::send(const void* data, const size_t size) const
{
  if (use_ssl)
  {
      Logger::log("[Connection] SSLsend:", Logger::ERROR);
      return SSL_write(ssl, data, size) > 0;
  }
  else
  {
    ssize_t bytes_sent_now = 0;

    for (size_t bytes_sent = 0L; bytes_sent < size; bytes_sent += bytes_sent_now)
    {
      bytes_sent_now = ::send(sockfd_, static_cast<const char*>(data) + bytes_sent, size - bytes_sent,
          MSG_NOSIGNAL | MSG_DONTWAIT);
      if(bytes_sent_now <= 0) {
        Logger::error(std::string("[Connection] send:") += strerror(errno));
        return false;
      }
    }
  }

  return true;
}

ssize_t Connection::recv(void *buf, const size_t size, bool wait) const
{
  if (use_ssl)
  {
    return SSL_read(ssl, buf, size);
  }
  else if (wait)
  {
    ssize_t bytes_received_now = 0;

    for (size_t bytes_received = 0L; bytes_received < size; bytes_received += bytes_received_now)
    {
      bytes_received_now = ::recv(sockfd_, static_cast<char*>(buf) + bytes_received, size - bytes_received, MSG_WAITALL);
      if( bytes_received_now <= 0 ) {
        Logger::error(std::string("[Connection] recv:") += strerror(errno));
        return bytes_received;
      }
    }

    return size;
  }

  return read(sockfd_, static_cast<char*>(buf), size);
}

bool Connection::isAlive(void) const
{
  Logger::debug("[Connection] isAlive() -> " + std::to_string(sockfd_ != -1));
  return sockfd_ != -1;
}


void Connection::shutdown(int how)
{
  ::shutdown(sockfd_, how);
}

void Connection::setDefaultTimeout(void)
{
  setTimeout(5);
}

void Connection::setTimeout(int seconds)
{
  timeval timeout = { seconds, 0 };

  if (setsockopt(sockfd_, SOL_SOCKET, SO_RCVTIMEO, &timeout, sizeof(timeout)))
    throw std::runtime_error(std::string("[Connection] cannot set timeout") += strerror(errno));
  if (setsockopt(sockfd_, SOL_SOCKET, SO_SNDTIMEO, &timeout, sizeof(timeout)))
    throw std::runtime_error(std::string("[Connection] cannot set timeout") += strerror(errno));
}

void Connection::connectSSL(void)
{
  ctx = SSL_CTX_new(SSLv23_client_method());
  if (ctx == nullptr) {
    ERR_print_errors_fp(stderr);
    throw std::runtime_error(std::string("[Connection] Cannot create SSL context"));
  }

  ssl =  SSL_new(ctx);
  if (ssl == nullptr) {
    ERR_print_errors_fp(stderr);
    throw std::runtime_error(std::string("[Connection] Cannot create SSL handler"));
  }

  if (!SSL_set_fd(ssl, sockfd_)) {
    ERR_print_errors_fp(stderr);
    throw std::runtime_error(std::string("[Connection] Cannot set SSL file descriptor"));
  }

  Logger::log("[Connection] Connecting SSL", Logger::VERBOSE);
  if (SSL_connect(ssl) != 1) {
    ERR_print_errors_fp(stderr);
    throw std::runtime_error(std::string("[Connection] Cannot establish SSL connection"));
  }
}

void Connection::acceptSSL(void)
{
  assert(ctx == nullptr);
  assert(ssl == nullptr);

  ctx = SSL_CTX_new(SSLv23_server_method());
  if (ctx == nullptr) {
    ERR_print_errors_fp(stderr);
    throw std::runtime_error(std::string("[Connection] Cannot create SSL context"));
  }

  if (SSL_CTX_use_certificate_file(ctx, cert, SSL_FILETYPE_PEM) <= 0) {
    ERR_print_errors_fp(stderr);
    throw std::runtime_error(std::string("[Connection] Cannot use server certificate"));
  }

  if (SSL_CTX_use_PrivateKey_file(ctx, cert_key, SSL_FILETYPE_PEM) <= 0) {
    ERR_print_errors_fp(stderr);
    throw std::runtime_error(std::string("[Connection] Cannot use server private key"));
  }

  ssl =  SSL_new(ctx);
  if (ssl == nullptr) {
    ERR_print_errors_fp(stderr);
    throw std::runtime_error(std::string("[Connection] Cannot create SSL handler"));
  }

  if (!SSL_set_fd(ssl, sockfd_)) {
    ERR_print_errors_fp(stderr);
    throw std::runtime_error(std::string("[Connection] Cannot set SSL file descriptor"));
  }

  if (SSL_accept(ssl)) {
    ERR_print_errors_fp(stderr);
    throw std::runtime_error(std::string("[Connection] Cannot accept SSL connection"));
  }
}

int Connection::id(void)
{
  return id_;
}

uint16_t Connection::getPort(void) const
{
  return ntohs(reinterpret_cast<const sockaddr_in*>(&address)->sin_port);
}

void Connection::waitForFullBuffer(const ssize_t  size) const
{
  char unused[size];
  ssize_t received_previously, received;
  while ((received = ::recv(sockfd_, unused, size, MSG_PEEK)))
  {
    if (received == -1 || received == size || received == received_previously) {
      break;
    }

    Logger::debug("Still receiving: " + std::to_string(received));
    received_previously = received;
    usleep(100000);
  }
}
