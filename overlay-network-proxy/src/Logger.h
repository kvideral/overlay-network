/*
 * Logger.h
 *
 *  Created on: 22. 2. 2015
 *      Author: Lukáš Kvídera
 */

#ifndef LOGGER_H_
#define LOGGER_H_

#include <string>

class Logger
{
	public:
		enum LogLevel { NONE = 0, ERROR = 1, NORMAL = 2, VERBOSE = 3, DEBUG = 4, TRACE = 5 };

		static void log(const std::string& msg, LogLevel loglevel);
		static void error(const std::string& msg);
		static void info(const std::string& msg);
		static void debug(const std::string& msg);
		static void trace(const std::string& msg);
		static LogLevel LOG_LEVEL;
};

#endif /* LOGGER_H_ */
