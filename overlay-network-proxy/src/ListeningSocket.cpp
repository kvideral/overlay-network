/*
 * ListeningSocket.cpp
 *
 *  Created on: 23. 9. 2015
 *      Author: Lukáš Kvídera
 */

#include <stdexcept>
#include <cerrno>

#include <unistd.h>
#include <arpa/inet.h>
#include <sys/socket.h>

#include "ListeningSocket.h"
#include "Connection.h"
#include "custom-types.h"

ListeningSocket::ListeningSocket(const port port, const int style, const int type)
{
  sockaddr_in6 sockaddr = {};

  sockaddr.sin6_family = AF_INET6;

  sockaddr.sin6_port = htons(port);
  sockaddr.sin6_addr = in6addr_any;

  if ((sock = socket(sockaddr.sin6_family, style, type)) == -1) {
    throw std::runtime_error("Cannot get socket!");
  }

  int opt = 1;
  if (setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof opt) < 0) {
    throw std::runtime_error("setsockopt(SO_REUSEADDR) failed");
  }

  if (bind(sock, reinterpret_cast<const struct sockaddr*>(&sockaddr), sizeof sockaddr) == -1) {
    throw std::runtime_error(std::string { "Cannot bind socket! " } + strerror(errno));
  }

  if (style == SOCK_STREAM && listen(sock, 100) == -1) {
    throw std::runtime_error("Cannot listen on socket! " + std::to_string(port));
  }
}

ListeningSocket::~ListeningSocket()
{
  close(sock);
}

Connection ListeningSocket::accept(void) const
{
  sockaddr_storage address;
  socklen_t len = sizeof(address);
  const int socket = ::accept(sock, reinterpret_cast<struct sockaddr*>(&address), &len);
  return { socket , address };
}

ssize_t ListeningSocket::recvfrom(void *buf, size_t size, sockaddr_storage *remote_address, socklen_t *addr_len) const
{
  return ::recvfrom(sock, buf, size, 0, reinterpret_cast<sockaddr*>(remote_address), addr_len);
}

ssize_t ListeningSocket::sendto(const void *buf, size_t size, const sockaddr_storage *remote_address, const socklen_t addr_len) const
{
  return ::sendto(sock, buf, size, 0, reinterpret_cast<const sockaddr*>(remote_address), addr_len);
}

void ListeningSocket::set_timeout(int seconds) {
    timeval tv { seconds };
    if (setsockopt(sock, SOL_SOCKET, SO_RCVTIMEO, &tv, sizeof tv) < 0) {
      throw std::runtime_error("setsockopt(SO_REUSEADDR) failed");
    }
}
