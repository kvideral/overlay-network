/*----------------------------------------------------------------------------
 * SQLiteDatabase.h
 *
 *  Created on: 28. 2. 2016
 *      Author: Lukáš Kvídera
 *      Jabber:  segfault@dione.zcu.cz
 ---------------------------------------------------------------------------*/
#ifndef SRC_TIMESERIESDATABASE_H_
#define SRC_TIMESERIESDATABASE_H_

#include <sqlite3.h>
#include <netinet/in.h>

/*
 *
 */
class TimeSeriesDatabase
{
  public:
    TimeSeriesDatabase();
    ~TimeSeriesDatabase();
    void insert_delay(const sockaddr_storage& address, clock_t delay);
    void insert_packetloss(const sockaddr_storage& address, uint32_t packets_lost);

  private:
    const char* db_path = "/tmp/overlay-network.sqlite3";
    sqlite3* db;
};

#endif /* SRC_TIMESERIESDATABASE_H_ */
