/*----------------------------------------------------------------------------
 * main.cpp
 *
 *  Created on: 20. 6. 2015
 *      Author: Lukáš Kvídera
 *      Jabber: segfault@dione.zcu.cz
 ---------------------------------------------------------------------------*/

#include <cstdio>

#include "ListeningSocket.h"
#include "ProxyPipeline.h"
#include "config.h"

int main(const int argc, const char *argv[])
{
  if (argc != 2) {
    std::perror("overlay-network <port>");
    return -1;
  }

  std::printf("HELLO: %d s PING: %d s UPDATE: %d s\n", HELLO_DELAY, PING_DELAY, UPDATE_DELAY);
  ProxyPipeline { argv[1] };
}
