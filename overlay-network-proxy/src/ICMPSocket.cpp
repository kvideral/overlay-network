/*----------------------------------------------------------------------------
 * ICMPSocket.cpp
 *
 *  Created on: 28. 12. 2015
 *      Author: Lukáš Kvídera
 *      Jabber:  segfault@dione.zcu.cz
 ---------------------------------------------------------------------------*/

#include <array>
#include <stdexcept>
#include <ctime>
#include <cstring>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/socket.h>

#include <netinet/in.h>
#include <linux/icmp.h>
#include <arpa/inet.h>

#include "ICMPSocket.h"
#include "Logger.h"

#include <iostream>

uint16_t checksum(uint16_t *buffer, size_t size);

ICMPSocket::ICMPSocket()
{
   icmp_socket = socket(AF_INET, SOCK_RAW, IPPROTO_ICMP);
   if (icmp_socket < 0) {
     throw std::runtime_error("Cannot open ICMP socket!");
   }

//   icmp_filter filter { 1<<ICMP_ECHOREPLY };
//   if (setsockopt(icmp_socket, SOL_RAW, ICMP_FILTER, &filter, sizeof filter) < 0) {
//     throw std::runtime_error("Cannot filter ICMP!");
//   }

   timeval timeout { 1 };
   if (setsockopt(icmp_socket, SOL_SOCKET, SO_RCVTIMEO, &timeout, sizeof timeout) < 0) {
     throw std::runtime_error("Cannot set recv timeout!");
   }
}

ICMPSocket::~ICMPSocket()
{
  close(icmp_socket);
}


clock_t ICMPSocket::probe(const sockaddr_storage& dest) const
{
  static uint16_t id { };
  const uint16_t expected_id = ++id;
  std::array<char, 52> buffer { };
  icmphdr& header = reinterpret_cast<icmphdr&>(buffer.front());
  header.type = ICMP_ECHO;
  header.un.echo.id = rand();
  header.un.echo.sequence = expected_id;
  header.checksum = checksum(reinterpret_cast<uint16_t*>(&header), sizeof header);

  clock_t start = std::clock();
  if (sendto(icmp_socket, &header, sizeof header, 0, reinterpret_cast<const sockaddr*>(&dest), sizeof dest) < 0) {
    std::perror("Cannot send ICMP ECHO!");
    return INT32_MAX;
  }

  if (recvfrom(icmp_socket, &header, buffer.size(), 0, nullptr, nullptr) < 0) {
    std::perror("Cannot receive ICMP REPLY!");
    return INT32_MAX;
  }

  /** We received IP header with ICMP header, so we have to add offset */
  header = reinterpret_cast<icmphdr&>(*(buffer.begin() + 20));
  if (header.type != ICMP_ECHOREPLY) {
    Logger::error("This wasn_t REPLY!");
    return INT32_MAX;
  }

  return std::clock() - start;
}

uint16_t checksum(uint16_t *buffer, size_t size)
{
  uint64_t cksum { };
  while(size > 1)
  {
    cksum += *buffer++;
    size -= sizeof(uint16_t);
  }

  if(size)
  {
    cksum += *(uint8_t*) buffer;
  }

  cksum = (cksum >> 16) + (cksum & 0xffff);
  cksum += (cksum >> 16);
  return ~cksum;
}
