/*----------------------------------------------------------------------------
 * utils.h
 *
 *  Created on: 22. 12. 2015
 *      Author: Lukáš Kvídera
 *      Jabber:  segfault@dione.zcu.cz
 ---------------------------------------------------------------------------*/
#ifndef SRC_UTILS_H_
#define SRC_UTILS_H_

#include <string>

const std::string to_string(const sockaddr_storage& address);
bool cmpaddr(const sockaddr_storage& first, const sockaddr_storage& second);

#endif /* SRC_UTILS_H_ */
