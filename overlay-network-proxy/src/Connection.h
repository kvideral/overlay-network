/*
 * Connection.h
 *
 *  Created on: 3. 2. 2015
 *      Author: Lukáš Kvídera
 */

#ifndef CONNECTION_H_
#define CONNECTION_H_

#include <string>
#include <atomic>
#include <memory>

#include <arpa/inet.h>
#include <sys/socket.h>
#include <openssl/ssl.h>
#include <openssl/err.h>


class Connection
{
  public:
    Connection(void);
    Connection(Connection&& connection);
    Connection(const Connection& connection) = delete;
    Connection(const sockaddr_storage& address, const bool use_ssl = false);
    Connection(const int sockfd, const sockaddr_storage& address, const bool use_ssl = false);
    Connection(const std::string& hostname, const std::string& port, const bool use_ssl = false);
    ~Connection(void);

    bool send(const void * buf, const size_t size) const;
    ssize_t recv(void * buf, const size_t size, bool wait = true) const;
    bool isAlive(void) const;
    void shutdown(int how);
    void setDefaultTimeout(void);
    void setTimeout(int seconds);
    void connectSSL(void);
    void acceptSSL(void);
    int id(void);
    uint16_t getPort(void) const;
    void waitForFullBuffer(const ssize_t size) const;

    Connection& operator =(Connection&& connection);
  private:
    static std::atomic<int> id_counter_;

    int sockfd_ = -1;
    int id_;
    SSL_CTX *ctx = nullptr;
    SSL *ssl = nullptr;
    sockaddr_storage address;
    const bool use_ssl = false;
};

using shared_connection = std::shared_ptr<Connection>;

#endif /* CONNECTION_H_ */
