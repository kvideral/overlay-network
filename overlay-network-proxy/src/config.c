/*----------------------------------------------------------------------------
 * config.c
 *
 *  Created on: 28. 12. 2015
 *      Author: Lukáš Kvídera
 *      Jabber:  segfault@dione.zcu.cz
 ---------------------------------------------------------------------------*/

#include <stdint.h>

const int HELLO_DELAY = 10;
const int PING_DELAY = 100;
const int UPDATE_DELAY = 3;
const uint8_t VERSION = 1;


