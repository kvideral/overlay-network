/*
 * ProxyTunnel.cpp
 *
 *  Created on: 3. 2. 2015
 *      Author: Lukáš Kvídera
 */

#include <cstring>
#include <stdexcept>
#include <array>
#include <memory>
#include <thread>

#include <unistd.h>
#include <tbb/tbb.h>

#include "ProxyTunnel.h"
#include "Connection.h"
#include "Logger.h"

ProxyTunnel::ProxyTunnel()
{
  std::thread client_to_server { &ProxyTunnel::wait_for_tunnels, this };
  std::thread server_to_client { &ProxyTunnel::wait_for_tunnels, this };
  client_to_server.detach();
  server_to_client.detach();
}

void forward(const shared_connection source, const shared_connection destination)
{
  Logger::info("forward()");
  std::array<char, 2 << 15> buf;

  bool stop = false;
  do {
    ssize_t received = source->recv(buf.begin(), buf.size(), false);
    Logger::debug("Received " + std::to_string(received) + " bytes");

    if (received == -1)
    {
      switch (errno)
      {
        case 0:
        case EINTR:
        case EAGAIN:
          errno = 0;
          continue;

        default:
          stop = true;
          break;
      }
    } else if (received == 0) {
      stop = true;
    } else {
      if (!destination->send(buf.begin(), received)) {
        stop = true;
      }
      Logger::debug("Sent " + std::to_string(received) + " bytes");
    }
  } while (!stop);

  /* We can do nothing about error, just stop forwarding */
  destination->shutdown(SHUT_WR);
  source->shutdown(SHUT_RD);
}

void ProxyTunnel::wait_for_tunnels()
{
  Logger::trace("wait_for_tunnels()");
  while (true)
  {
    tunnel tunnel;
    tunnels.pop(tunnel);
    Logger::trace("free threads " + std::to_string(free_threads));
    if (--free_threads < 2) {
      ++free_threads;
      Logger::trace("created a new threadf");
      std::thread forwarder { &ProxyTunnel::wait_for_tunnels, this };
      forwarder.detach();
    }

    ::forward(tunnel.first, tunnel.second);

    if (free_threads > 64) {
      return;
    }

    ++free_threads;
  }
}

void ProxyTunnel::forward(const shared_connection& client, const shared_connection& server)
{
  Logger::debug("[PoxyTunnel] forward(client, server)");
  tunnels.push(tunnel { client, server });
  tunnels.push(tunnel { server, client });
}
