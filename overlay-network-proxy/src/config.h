/*----------------------------------------------------------------------------
 * config.h
 *
 *  Created on: 28. 12. 2015
 *      Author: Lukáš Kvídera
 *      Jabber:  segfault@dione.zcu.cz
 ---------------------------------------------------------------------------*/
#ifndef SRC_CONFIG_H_
#define SRC_CONFIG_H_

#include <stdint.h>

extern int HELLO_DELAY;
extern int PING_DELAY;
extern int UPDATE_DELAY;
extern uint8_t VERSION;


#endif /* SRC_CONFIG_H_ */
