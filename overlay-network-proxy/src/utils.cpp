/*----------------------------------------------------------------------------
 * utils.cpp
 *
 *  Created on: 22. 12. 2015
 *      Author: Lukáš Kvídera
 *      Jabber:  segfault@dione.zcu.cz
 ---------------------------------------------------------------------------*/


#include <string>
#include <cstring>
#include <arpa/inet.h>

#include "Router.h"
#include "utils.h"


const std::string to_string(const sockaddr_storage& address)
{
  return inet_ntoa(reinterpret_cast<const sockaddr_in&>(address).sin_addr);
}


bool cmpaddr(const sockaddr_storage& first, const sockaddr_storage& second)
{
  if (first.ss_family != second.ss_family) {
    return false;
  }

  switch (first.ss_family) {
    case AF_INET:
      return std::memcmp(
          &reinterpret_cast<const sockaddr_in&>(first).sin_addr,
          &reinterpret_cast<const sockaddr_in&>(second).sin_addr,
          sizeof reinterpret_cast<const sockaddr_in&>(second).sin_addr
          ) == 0;

    case AF_INET6:
      return std::memcmp(
         &reinterpret_cast<const sockaddr_in6&>(first).sin6_addr,
         &reinterpret_cast<const sockaddr_in6&>(second).sin6_addr,
         sizeof reinterpret_cast<const sockaddr_in6&>(second).sin6_addr
         ) == 0;

    default:
      return false;
  }

}
