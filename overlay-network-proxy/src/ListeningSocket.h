/*
 * ListeningSocket.h
 *
 *  Created on: 23. 9. 2015
 *      Author: Lukáš Kvídera
 */

#ifndef SRC_LISTENINGSOCKET_H_
#define SRC_LISTENINGSOCKET_H_

#include "Connection.h"
#include "custom-types.h"

class ListeningSocket
{
  public:
    ListeningSocket(const port port,const int style = SOCK_STREAM, const int type = IPPROTO_TCP);
    ~ListeningSocket();

    void set_timeout(int seconds);
    Connection accept(void) const;
    ssize_t recvfrom(void *buf, size_t size, sockaddr_storage *remote_address, socklen_t *addr_len) const;
    ssize_t sendto(const void *buf, size_t size,
        const sockaddr_storage *remote_address, const socklen_t addr_len) const;

  private:
    int sock;
};

#endif /* SRC_LISTENINGSOCKET_H_ */
