#!/bin/bash

IP_BEGIN="172.17.0"


for ts in {0..1000}
do
  echo "10" >> /tmp/base.ts
done

for ip1 in {1..10}
do
  for ip2 in {1..10}
  do
    if [ $ip1 -eq $ip2 ]; then
      continue
    fi
  
    ln /tmp/base.ts /tmp/${IP_BEGIN}.${ip1}-${IP_BEGIN}.${ip2}.ts
  done
done
