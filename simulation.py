#!/usr/bin/python3

import sys
from subprocess import call
from time import sleep

def getDelayCmd(dev, flowId, delay):
  return (
    "tc qdisc add dev "
    + dev +
    " parent 1:"
    + str(flowId) +
    " handle " 
    + str(flowId) +
    ": netem loss 0% 1% delay "
    + str(delay) +
    "ms 10ms"
    )

def getFilterCmd(dev, flowId, ipFrom):
  return (
    "tc filter add dev "
     + dev +
     " protocol ip prio 1 u32 match ip src "
     + ipFrom +
     " flowid 1:"
     + str(flowId)
     )

def getClassCmd(dev, flowId, throughput):
  return (
    "tc class add dev "
    + dev + 
    " parent 1: classid 1:"
    + str(flowId) + 
    " htb rate " + str(throughput) + "Mbps"
  )


def getKey(ipFrom, ipTo):
  return ipFrom + "-" + ipTo

def parseArgs():
  args = sys.argv[1:]
  for index in range(0, len(args) - 1, 2):
    devices[args[index]] = args[index + 1]

def load():
  for deviceFrom, ipFrom in devices.items():
    flowId = 10
    for deviceTo, ipTo in devices.items():
      if deviceFrom == deviceTo:
        continue

      key = getKey(ipFrom, ipTo)
      flowIds[key] = flowId
      flowId += 1

      with open("/tmp/" + key + ".delay.ts") as delaySerie:
        delays[key] = delaySerie.read().splitlines()
  
      with open("/tmp/" + key + ".throughput.ts") as throughputSerie:
        throughputs[key] = throughputSerie.read().splitlines()


def run():
  for minute in range(0, len(next(iter(delays.values())))):
    for deviceTo, ipTo in devices.items():
     call(["tc", "qdisc", "del", "dev", deviceTo, "root"])
     call(["tc", "qdisc", "add", "dev", deviceTo, "root", "handle", "1:", "htb"])
     for deviceFrom, ipFrom in devices.items():
       if deviceFrom == deviceTo:
        continue

       key = getKey(ipTo, ipFrom)
       flowId = flowIds[key]
       delay = delays[key][minute]
       throughput = throughputs[key][minute]

       print(ipFrom + " -> " + ipTo + " " + delay + " ms" + " " + throughput + "Mbps")
       call(getClassCmd(deviceTo, flowId, throughput).split(" "))
       call(getDelayCmd(deviceTo, flowId, delay).split(" "))
       call(getFilterCmd(deviceTo, flowId, ipFrom).split(" "))
    sleep(10)



devices = {}
delays = {}
throughputs = {}
flowIds = {}

parseArgs()
load()
run()
